import "../../../../index.css";

const ExerciseInfoForm = () => {
  return (
    <form action="exercise-info" method="get">
      <div className="ttl">Введите название файла наблюдений</div>
      <label htmlFor="filename">Название файла</label>
      <input name="filename" type="text" id="filename" placeholder="Введите текст" />
      <button className="form-button" type="submit">Отправить</button>
    </form>
  );
}

export default ExerciseInfoForm;