import { render, unmountComponentAtNode } from "react-dom";
import ExerciseInfoForm from "./ExerciseInfoForm";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldGiveInputForm", () => {
  render(<ExerciseInfoForm />, container);

  const actual = container.getElementsByClassName("ttl")[0].textContent;
  const expected = "Введите название файла наблюдений";
  expect(actual).toBe(expected);
});