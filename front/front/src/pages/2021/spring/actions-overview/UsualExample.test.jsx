import { render, unmountComponentAtNode } from "react-dom";
import UsualExample from "./UsualExample";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppDownloadMainExampleButton", () => {
  render(<UsualExample fileName="mainExample.cpp" />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "Загрузить mainExample.cpp";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonDownloadMainExampleButton", () => {
  render(<UsualExample fileName="mainExample.py" />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "Загрузить mainExample.py";
  expect(actual).toBe(expected);
});