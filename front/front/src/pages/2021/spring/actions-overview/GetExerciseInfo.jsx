const GetExerciseInfo = () => {
  return (
    <a href="http://localhost:3000/year/2021/spring/exercise-info-form">
      <button className="overview" type="submit">Получить информацию по заданию</button>
    </a>
  );
}

export default GetExerciseInfo;