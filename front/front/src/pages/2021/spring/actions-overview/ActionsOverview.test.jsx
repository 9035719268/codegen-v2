import { render, unmountComponentAtNode } from "react-dom";
import ActionsOverview from "./ActionsOverview";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldReturnValuesForCppFileNoOop", () => {
  const location = {
    search: "?lang=cpp&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForCppFileWithOop", () => {
  const location = {
    search: "?lang=cpp&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForPythonFileNoOop", () => {
  const location = {
    search: "?lang=python&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForPythonFileWithOop", () => {
  const location = {
    search: "?lang=python&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForJavaFileWithOop", () => {
  const location = {
    search: "?lang=java&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Java, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить Main.java";

  const actualDownloadGraphDrawerButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadGraphDrawerButton = "Загрузить GraphDrawer.java";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[2].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[3].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[4].textContent;
  const expectedDownloadMainExampleButton = "Загрузить MainExample.java";

  const actualDownloadGraphDrawerExampleButton = container.getElementsByTagName("A")[5].textContent;
  const expectedDownloadGraphDrawerExampleButton = "Загрузить GraphDrawerExample.java";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadGraphDrawerButton).toBe(expectedDownloadGraphDrawerButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
  expect(actualDownloadGraphDrawerExampleButton).toBe(expectedDownloadGraphDrawerExampleButton);
});

it("shouldReturnValuesForCppManualNoOop", () => {
  const location = {
    search: "?lang=cpp&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForCppManualWithOop", () => {
  const location = {
    search: "?lang=cpp&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForPythonManualNoOop", () => {
  const location = {
    search: "?lang=python&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForPythonManualWithOop", () => {
  const location = {
    search: "?lang=python&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForJavaManualWithOop", () => {
  const location = {
    search: "?lang=java&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Весна, 2021 год";

  const actualLangText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedLangText = "Язык программирования: Java, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить Main.java";

  const actualDownloadGraphDrawerButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadGraphDrawerButton = "Загрузить GraphDrawer.java";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[2].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[3].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[4].textContent;
  const expectedDownloadMainExampleButton = "Загрузить MainExample.java";

  const actualDownloadGraphDrawerExampleButton = container.getElementsByTagName("A")[5].textContent;
  const expectedDownloadGraphDrawerExampleButton = "Загрузить GraphDrawerExample.java";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadGraphDrawerButton).toBe(expectedDownloadGraphDrawerButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
  expect(actualDownloadGraphDrawerExampleButton).toBe(expectedDownloadGraphDrawerExampleButton);
});