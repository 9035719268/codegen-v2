const DownloadResourcesButton = () => {
  return (
    <a href="http://localhost:8080/year/2021/spring/download/resources" download="resources.rar">
      <button className="overview" type="submit">Загрузить resources.rar</button>
    </a>
  );
}

export default DownloadResourcesButton;