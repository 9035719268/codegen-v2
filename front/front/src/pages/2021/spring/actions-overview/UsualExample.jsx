const UsualExample = props => {
  return (
    <a href={props.path + props.fileName} download={props.fileName}>
      <button type="submit">Загрузить {props.fileName}</button>
    </a>
  );
}

export default UsualExample;