import JavaExample from "./JavaExample";
import UsualExample from "./UsualExample";

const DownloadExampleCodeButton = props => {
  const PATH = "http://localhost:8080/year/2021/spring/download/" +
    props.lang + "/" +
    props.fileParam + "/" +
    props.oopParam + "/";

  const fileName = props.lang === "python" ? "mainExample.py" : "mainExample.cpp";

  if (props.lang === "java") {
    return <JavaExample path={PATH} />
  } else {
    return <UsualExample path={PATH} fileName={fileName} />
  }
}

export default DownloadExampleCodeButton;