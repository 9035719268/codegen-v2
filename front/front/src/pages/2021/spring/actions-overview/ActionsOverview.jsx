import "../../../../index.css";
import DownloadCodeButton from "./DownloadCodeButton";
import DownloadResourcesButton from "./DownloadResourcesButton";
import GetExerciseInfo from "./GetExerciseInfo";
import DownloadExampleCodeButton from "./DownloadExampleCodeButton";
import { extractFileParam, extractLang, extractOopParam } from "../service/queryParamsExtraction";

const ActionsOverview = props => {
  const routerSearch = props.location.search;
  const lang = extractLang(routerSearch);
  const fileParam = extractFileParam(routerSearch);
  const oopParam = extractOopParam(routerSearch);

  return (
    <div>
      <div className="ttl">
        <p>Весна, 2021 год</p>
        <span>Язык программирования: {lang.charAt(0).toUpperCase() + lang.slice(1)}, </span>
        <span>{fileParam === "file" ? "с чтением файла, " : "без чтения файла, "}</span>
        <span>{oopParam === "withoop" ? "с ООП" : "без ООП"}</span>
      </div>
      <DownloadCodeButton lang={lang} fileParam={fileParam} oopParam={oopParam} />
      <DownloadResourcesButton />
      <GetExerciseInfo />
      <DownloadExampleCodeButton lang={lang} fileParam={fileParam} oopParam={oopParam} />
    </div>
  );
}

export default ActionsOverview;