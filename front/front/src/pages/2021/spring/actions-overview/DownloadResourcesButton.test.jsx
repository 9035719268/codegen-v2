import { render, unmountComponentAtNode } from "react-dom";
import DownloadResourcesButton from "./DownloadResourcesButton";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderGetExerciseInfoButton", () => {
  render(<DownloadResourcesButton />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "Загрузить resources.rar";
  expect(actual).toBe(expected);
});