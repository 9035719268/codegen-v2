import { render, unmountComponentAtNode } from "react-dom";
import JavaCode from "./JavaCode";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadMainButton", () => {
  render(<JavaCode />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "Загрузить Main.java";
  expect(actual).toBe(expected);
});

it("shouldRenderDownloadGraphDrawerButton", () => {
  render(<JavaCode />, container);

  const actual = container.getElementsByTagName("BUTTON")[1].textContent;
  const expected = "Загрузить GraphDrawer.java";
  expect(actual).toBe(expected);
});