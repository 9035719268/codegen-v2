const TableHeader = () => {
  return (
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">С чтением файла (на 5)</th>
        <th scope="col">Без чтения файла (на 3/4)</th>
      </tr>
    </thead>
  );
}

export default TableHeader;