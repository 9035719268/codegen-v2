import { render, unmountComponentAtNode } from "react-dom";
import TableRow from "./TableRow";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppRow", () => {
  render(<TableRow lang="cpp" />, container);

  const actualHeader = container.getElementsByTagName("TH")[0].textContent;
  const expectedHeader = "Cpp";

  const actualFirstButton = container.getElementsByTagName("BUTTON")[0].textContent;
  const expectedFirstButton = "Без использования ООП";

  const actualSecondButton = container.getElementsByTagName("BUTTON")[1].textContent;
  const expectedSecondButton = "С использованием ООП";

  expect(actualHeader).toBe(expectedHeader);
  expect(actualFirstButton).toBe(expectedFirstButton);
  expect(actualSecondButton).toBe(expectedSecondButton);
});

it("shouldRenderPythonRow", () => {
  render(<TableRow lang="python" />, container);

  const actualHeader = container.getElementsByTagName("TH")[0].textContent;
  const expectedHeader = "Python";

  const actualFirstButton = container.getElementsByTagName("BUTTON")[0].textContent;
  const expectedFirstButton = "Без использования ООП";

  const actualSecondButton = container.getElementsByTagName("BUTTON")[1].textContent;
  const expectedSecondButton = "С использованием ООП";

  expect(actualHeader).toBe(expectedHeader);
  expect(actualFirstButton).toBe(expectedFirstButton);
  expect(actualSecondButton).toBe(expectedSecondButton);
});

it("shouldRenderJavaRow", () => {
  render(<TableRow lang="java" />, container);

  const actualHeader = container.getElementsByTagName("TH")[0].textContent;
  const expectedHeader = "Java";

  const actualFirstButton = container.getElementsByTagName("BUTTON")[0].textContent;
  const expectedFirstButton = "С использованием ООП";

  expect(actualHeader).toBe(expectedHeader);
  expect(actualFirstButton).toBe(expectedFirstButton);
});