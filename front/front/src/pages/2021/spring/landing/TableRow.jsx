import TableQuery from "./TableQuery";

const TableRow = props => {
  const PATH = "/year/2021/spring/actions-overview?";
  const WITH_OOP = "С использованием ООП";
  const NO_OOP = "Без использования ООП";

  const RenderOnlyOop = () => {
    return (
      <tr>
        <th scope="row">{props.lang.charAt(0).toUpperCase() + props.lang.slice(1)}</th>
        <td>
          <TableQuery
            path={PATH}
            lang={props.lang}
            fileParam="true"
            oopParam="true"
            text={WITH_OOP}
          />
        </td>
        <td>
          <TableQuery
            path={PATH}
            lang={props.lang}
            fileParam="false"
            oopParam="true"
            text={WITH_OOP}
          />
        </td>
      </tr>
    );
  }

  const RenderUsual = () => {
    return (
      <tr>
        <th scope="row">{props.lang.charAt(0).toUpperCase() + props.lang.slice(1)}</th>
        <td>
          <TableQuery
            path={PATH}
            lang={props.lang}
            fileParam="true"
            oopParam="false"
            text={NO_OOP}
          />
          <TableQuery
            path={PATH}
            lang={props.lang}
            fileParam="true"
            oopParam="true"
            text={WITH_OOP}
          />
        </td>
        <td>
          <TableQuery
            path={PATH}
            lang={props.lang}
            fileParam="false"
            oopParam="false"
            text={NO_OOP}
          />
          <TableQuery
            path={PATH}
            lang={props.lang}
            fileParam="false"
            oopParam="true"
            text={WITH_OOP}
          />
        </td>
      </tr>
    );
  }

  if (props.lang === "java") {
    return <RenderOnlyOop />
  } else {
    return <RenderUsual />
  }
}

export default TableRow;