import { render, unmountComponentAtNode } from "react-dom";
import Landing from "./Landing";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderFileHeader", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[1].textContent;
  const expected = "С чтением файла (на 5)";
  expect(actual).toBe(expected);
});

it("shouldRenderManualHeader", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[2].textContent;
  const expected = "Без чтения файла (на 3/4)";
  expect(actual).toBe(expected);
});

it("shouldRenderCppHeader", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[3].textContent;
  const expected = "Cpp";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonHeader", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[4].textContent;
  const expected = "Python";
  expect(actual).toBe(expected);
});

it("shouldRenaderJavaHeader", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[5].textContent;
  const expected = "Java";
  expect(actual).toBe(expected);
});

it("shouldRenderCppFileNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "Без использования ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderCppFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[1].textContent;
  const expected = "С использованием ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderCppManualNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[2].textContent;
  const expected = "Без использования ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderCppManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[3].textContent;
  const expected = "С использованием ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonFileNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[4].textContent;
  const expected = "Без использования ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[5].textContent;
  const expected = "С использованием ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonManualNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[6].textContent;
  const expected = "Без использования ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[7].textContent;
  const expected = "С использованием ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderJavaFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[8].textContent;
  const expected = "С использованием ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderJavaManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[9].textContent;
  const expected = "С использованием ООП";
  expect(actual).toBe(expected);
});