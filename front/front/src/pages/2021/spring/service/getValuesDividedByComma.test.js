import getValuesDividedByComma from "./getValuesDividedByComma";

it("shouldDivideOneDigitValuesByComma", () => {
  const actual = getValuesDividedByComma(["1", "2", "3", "4"]);
  const expected = ["1, ", "2, ", "3, ", "4, "];
  expect(actual).toStrictEqual(expected);
});

it("shouldDivideMultipleDigitValuesByComma", () => {
  const actual = getValuesDividedByComma(["1", "23", "456", "7890", "12345", "67890"]);
  const expected = ["1, ", "23, ", "456, ", "7890, ", "12345, ", "67890, "];
  expect(actual).toStrictEqual(expected);
});

