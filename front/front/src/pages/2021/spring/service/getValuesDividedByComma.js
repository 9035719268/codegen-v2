const getValuesDividedByComma = oldValues => {
  let dividedValues = null;
  if (typeof oldValues !== "undefined") {
    dividedValues = oldValues.map(value => value + ", ")
  }
  return dividedValues;
}

export default getValuesDividedByComma;