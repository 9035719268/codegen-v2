import {
  extractLang, extractFileParam, extractOopParam, extractFileName
} from "./queryParamsExtraction"

it("shouldGetLang", () => {
  const actual = extractLang("?lang=cpp");
  const expected = "cpp";
  expect(actual).toBe(expected);
});

it("shouldGetFile", () => {
  const actual = extractFileParam("?file=true");
  const expected = "file";
  expect(actual).toBe(expected);
});

it("shouldGetManual", () => {
  const actual = extractFileParam("?file=false");
  const expected = "manual";
  expect(actual).toBe(expected);
});

it("shouldGetWithOop", () => {
  const actual = extractOopParam("?oop=true");
  const expected = "withoop";
  expect(actual).toBe(expected);
});

it("shouldGetNoOop", () => {
  const actual = extractOopParam("?oop=false");
  const expected = "nooop";
  expect(actual).toBe(expected);
});

it("shouldGetFileName", () => {
  const actual = extractFileName("?filename=arti_6hours.dat");
  const expected = "arti_6hours.dat";
  expect(actual).toBe(expected);
});

it("shouldGetAllParams", () => {
  const routerSearch = "?lang=cpp&file=true&oop=false&filename=arti_6hours.dat";

  const actualLang = extractLang(routerSearch);
  const expectedLang = "cpp";

  const actualFileParam = extractFileParam(routerSearch);
  const expectedFileParam = "file";

  const actualOopParam = extractOopParam(routerSearch);
  const expectedOopParam = "nooop";

  const actualFileName = extractFileName(routerSearch);
  const expectedFileName = "arti_6hours.dat";

  expect(actualLang).toBe(expectedLang);
  expect(actualFileParam).toBe(expectedFileParam);
  expect(actualOopParam).toBe(expectedOopParam);
  expect(actualFileName).toBe(expectedFileName);
})