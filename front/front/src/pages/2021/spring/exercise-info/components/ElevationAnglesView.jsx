import getValuesDividedByComma from "../../service/getValuesDividedByComma";

const ElevationAnglesView = props => {
  const elevation = getValuesDividedByComma(props.elevation);

  return (
    <div className="groupval">Углы возвышения {props.order} спутника:
      <div className="inf">{elevation}</div>
    </div>
  );
}

export default ElevationAnglesView;