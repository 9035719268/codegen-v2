import getValuesDividedByComma from "../../service/getValuesDividedByComma";

const TroposphericDelayComponentsView = props => {
  const md = getValuesDividedByComma(props.md);
  const td = getValuesDividedByComma(props.td);
  const mw = getValuesDividedByComma(props.mw);
  const tw = getValuesDividedByComma(props.tw);

  return (
    <div>
      <div className="groupval">Компонент md {props.order} спутника:
        <div className="inf">{md}</div>
      </div>
      <div className="groupval">Компонент td {props.order} спутника:
        <div className="inf">{td}</div>
      </div>
      <div className="groupval">Компонент mw {props.order} спутника:
        <div className="inf">{mw}</div>
      </div>
      <div className="groupval">Компонент tw {props.order} спутника:
        <div className="inf">{tw}</div>
      </div>
    </div>
  );
}

export default TroposphericDelayComponentsView;