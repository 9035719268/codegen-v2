import { render, unmountComponentAtNode } from "react-dom";
import FileNameView from "./FileNameView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderFileNameView", () => {
  render(<FileNameView fileName="arti_6hours.dat" />, container);

  const actual = container.textContent;
  const expected = "Название файла:arti_6hours.dat";
  expect(actual).toBe(expected);
});