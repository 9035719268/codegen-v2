import { render, unmountComponentAtNode } from "react-dom";
import ElevationAnglesView from "./ElevationAnglesView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderElevationAnglesView", () => {
  const order = "первого";
  const elevation = ["1", "2", "3", "4", "5"];
  render(<ElevationAnglesView order={order} elevation={elevation} />, container);

  const actual = container.textContent;
  const expected = "Углы возвышения первого спутника:1, 2, 3, 4, 5, ";
  expect(actual).toBe(expected);
});