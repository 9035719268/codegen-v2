const FileNameView = props => {
  return (
    <div className="groupval">
      <div>Название файла:
        <span className="inf">{props.fileName}</span>
      </div>
    </div>
  );
}

export default FileNameView;