import { render, unmountComponentAtNode } from "react-dom";
import ExerciseInfo from "./ExerciseInfo";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderExerciseInfoTitle", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("ttl")[0].textContent;
  const expected = "Дополнительная информация";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoFileNameLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[0].textContent;
  const expected = "Название файла:arti_6hours.dat";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatelliteNumbersLines", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const satelliteNumbersGroup = container.getElementsByClassName("groupval")[1];

  const actualSatellite1NumberLine = satelliteNumbersGroup.getElementsByTagName("DIV")[0].textContent;
  const expectedSatellite1NumberLine = "Номер первого спутника:7";

  const actualSatellite2NumberLine = satelliteNumbersGroup.getElementsByTagName("DIV")[1].textContent;
  const expectedSatellite2NumberLine = "Номер второго спутника:14";

  const actualSatellite3NumberLine = satelliteNumbersGroup.getElementsByTagName("DIV")[2].textContent;
  const expectedSatellite3NumberLine = "Номер третьего спутника:21";

  expect(actualSatellite1NumberLine).toBe(expectedSatellite1NumberLine);
  expect(actualSatellite2NumberLine).toBe(expectedSatellite2NumberLine);
  expect(actualSatellite3NumberLine).toBe(expectedSatellite3NumberLine);
});

it("shouldRenderExerciseInfoSatellite1MdLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[2].textContent;
  const expected = "Компонент md первого спутника:1, 2, 3, 4, 5, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite1TdLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[3].textContent;
  const expected = "Компонент td первого спутника:6, 7, 8, 9, 10, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite1MwLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[4].textContent;
  const expected = "Компонент mw первого спутника:11, 12, 13, 14, 15, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite1TwLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[5].textContent;
  const expected = "Компонент tw первого спутника:16, 17, 18, 19, 20, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite2MdLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[6].textContent;
  const expected = "Компонент md второго спутника:26, 27, 28, 29, 30, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite2TdLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[7].textContent;
  const expected = "Компонент td второго спутника:31, 32, 33, 34, 35, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite2MwLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[8].textContent;
  const expected = "Компонент mw второго спутника:36, 37, 38, 39, 40, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite2TwLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[9].textContent;
  const expected = "Компонент tw второго спутника:41, 42, 43, 44, 45, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite3MdLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[10].textContent;
  const expected = "Компонент md третьего спутника:51, 52, 53, 54, 55, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite3TdLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[11].textContent;
  const expected = "Компонент td третьего спутника:56, 57, 58, 59, 60, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite3MwLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[12].textContent;
  const expected = "Компонент mw третьего спутника:61, 62, 63, 64, 65, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite3TwLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[13].textContent;
  const expected = "Компонент tw третьего спутника:66, 67, 68, 69, 70, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite1ElevationLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[14].textContent;
  const expected = "Углы возвышения первого спутника:21, 22, 23, 24, 25, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite2ElevationLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[15].textContent;
  const expected = "Углы возвышения второго спутника:46, 47, 48, 49, 50, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite3ElevationLine", () => {
  const satellite1Data = {
    number: 7,
    md: [1, 2, 3, 4, 5],
    td: [6, 7, 8, 9, 10],
    mw: [11, 12, 13, 14, 15],
    tw: [16, 17, 18, 19, 20],
    elevation: [21, 22, 23, 24, 25]
  };

  const satellite2Data = {
    number: 14,
    md: [26, 27, 28, 29, 30],
    td: [31, 32, 33, 34, 35],
    mw: [36, 37, 38, 39, 40],
    tw: [41, 42, 43, 44, 45],
    elevation: [46, 47, 48, 49, 50]
  };

  const satellite3Data = {
    number: 21,
    md: [51, 52, 53, 54, 55],
    td: [56, 57, 58, 59, 60],
    mw: [61, 62, 63, 64, 65],
    tw: [66, 67, 68, 69, 70],
    elevation: [71, 72, 73, 74, 75]
  };

  const fileName = "arti_6hours.dat";

  render(
    <ExerciseInfo
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
      fileName={fileName}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[16].textContent;
  const expected = "Углы возвышения третьего спутника:71, 72, 73, 74, 75, ";
  expect(actual).toBe(expected);
});