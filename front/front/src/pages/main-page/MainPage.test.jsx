import { render, unmountComponentAtNode } from "react-dom";
import MainPage from "./MainPage";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderButtonForAutumn2020", () => {
  render(<MainPage />, container);
  const actualTextContent = container.getElementsByTagName("BUTTON")[0].textContent;
  const expectedTextContent = "Осень 2020";
  expect(actualTextContent).toBe(expectedTextContent);
});

it("shouldRenderButtonForSpring2021", () => {
  render(<MainPage />, container);
  const actualTextContent = container.getElementsByTagName("BUTTON")[1].textContent;
  const expectedTextContent = "Весна 2021";
  expect(actualTextContent).toBe(expectedTextContent);
});