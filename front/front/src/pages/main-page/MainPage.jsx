const MainPage = () => {
  return (
    <div>
      <a href="/year/2020/autumn">
        <button className="overview" type="submit">Осень 2020</button>
      </a>
      <a href="/year/2021/spring">
        <button className="overview" type="submit">Весна 2021</button>
      </a>
    </div>
  );
}

export default MainPage;