import "../../../../index.css";
import { extractVarNumber } from "../service/queryParamsExtraction";
import Var3Form from "./Var3Form";
import UsualVarForm from "./UsualVarForm";

const ExerciseInfoForm = props => {
  const routerSearch = props.location.search;
  const varNumber = extractVarNumber(routerSearch);

  if (varNumber === "3") {
    return <Var3Form />
  } else {
    return <UsualVarForm varNumber={varNumber} />
  }
}

export default ExerciseInfoForm;