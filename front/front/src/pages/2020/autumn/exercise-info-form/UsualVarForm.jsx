const UsualVarForm = props => {
  return (
    <form action="exercise-info" method="get">
      <input type="hidden" name="var" value={props.varNumber} />
      <div className="ttl">Введите название файла наблюдений</div>
      <label htmlFor="filename">Название файла</label>
      <input name="filename" type="text" id="filename" placeholder="Введите текст" />
      <button className="form-button" type="submit">Отправить</button>
    </form>
  );
}

export default UsualVarForm;