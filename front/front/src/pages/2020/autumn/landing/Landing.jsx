import "bootstrap/dist/css/bootstrap.min.css";
import TableHeader from "./TableHeader";
import TableRow from "./TableRow";

const Landing = () => {
  return (
    <div>
      <table className="table table-hover table-dark">
        <TableHeader />
        <tbody>
          <TableRow lang="cpp" />
          <TableRow lang="python" />
          <TableRow lang="java" />
        </tbody>
      </table>
    </div>
  );
}

export default Landing;