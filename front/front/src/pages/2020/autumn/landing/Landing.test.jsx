import { render, unmountComponentAtNode } from "react-dom";
import Landing from "./Landing";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderVar1Header", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[1].textContent;
  const expected = "Вариант 1";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2Header", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[2].textContent;
  const expected = "Вариант 2";
  expect(actual).toBe(expected);
});

it("shouldRenderVar3Header", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[3].textContent;
  const expected = "Вариант 3";
  expect(actual).toBe(expected);
});

it("shouldRenderCppHeader", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[4].textContent;
  const expected = "Cpp";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonHeader", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[5].textContent;
  const expected = "Python";
  expect(actual).toBe(expected);
});

it("shouldRenaderJavaHeader", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("TH")[6].textContent;
  const expected = "Java";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1CppManualNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "На 3/4 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1CppManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[1].textContent;
  const expected = "На 3/4 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1CppFileNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[2].textContent;
  const expected = "На 5 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1CppFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[3].textContent;
  const expected = "На 5 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2CppManualNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[4].textContent;
  const expected = "На 3/4 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2CppManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[5].textContent;
  const expected = "На 3/4 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2CppFileNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[6].textContent;
  const expected = "На 5 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2CppFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[7].textContent;
  const expected = "На 5 с ООП";
  expect(actual).toBe(expected);
});


it("shouldRenderVar3CppManualNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[8].textContent;
  const expected = "На 3/4 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar3CppManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[9].textContent;
  const expected = "На 3/4 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar3CppFileNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[10].textContent;
  const expected = "На 5 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar3CppFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[11].textContent;
  const expected = "На 5 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1PythonManualNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[12].textContent;
  const expected = "На 3/4 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1PythonManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[13].textContent;
  const expected = "На 3/4 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1PythonFileNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[14].textContent;
  const expected = "На 5 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1PythonFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[15].textContent;
  const expected = "На 5 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2PythonManualNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[16].textContent;
  const expected = "На 3/4 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2PythonManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[17].textContent;
  const expected = "На 3/4 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2PythonFileNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[18].textContent;
  const expected = "На 5 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2PythonFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[19].textContent;
  const expected = "На 5 с ООП";
  expect(actual).toBe(expected);
});


it("shouldRenderVar3PythonManualNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[20].textContent;
  const expected = "На 3/4 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar3PythonManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[21].textContent;
  const expected = "На 3/4 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar3PythonFileNoOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[22].textContent;
  const expected = "На 5 без ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar3PythonFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[23].textContent;
  const expected = "На 5 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1JavaManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[24].textContent;
  const expected = "На 3/4 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar1JavaFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[25].textContent;
  const expected = "На 5 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2JavaManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[26].textContent;
  const expected = "На 3/4 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar2JavaFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[27].textContent;
  const expected = "На 5 с ООП";
  expect(actual).toBe(expected);
});


it("shouldRenderVar3JavaManualWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[28].textContent;
  const expected = "На 3/4 с ООП";
  expect(actual).toBe(expected);
});

it("shouldRenderVar3JavaFileWithOopButton", () => {
  render(<Landing />, container);

  const actual = container.getElementsByTagName("BUTTON")[29].textContent;
  const expected = "На 5 с ООП";
  expect(actual).toBe(expected);
});