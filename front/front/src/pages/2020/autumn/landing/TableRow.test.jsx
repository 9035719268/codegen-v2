import { render, unmountComponentAtNode } from "react-dom";
import TableRow from "./TableRow";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppRow", () => {
  render(<TableRow lang="cpp" />, container);

  const actualHeader = container.getElementsByTagName("TH")[0].textContent;
  const expectedHeader = "Cpp";

  const actualFirstButton = container.getElementsByTagName("BUTTON")[0].textContent;
  const expectedFirstButton = "На 3/4 без ООП";

  const actualSecondButton = container.getElementsByTagName("BUTTON")[1].textContent;
  const expectedSecondButton = "На 3/4 с ООП";

  const actualThirdButton = container.getElementsByTagName("BUTTON")[2].textContent;
  const expectedThirdButton = "На 5 без ООП";

  const actualFourthButton = container.getElementsByTagName("BUTTON")[3].textContent;
  const expectedFourthButton = "На 5 с ООП";

  expect(actualHeader).toBe(expectedHeader);
  expect(actualFirstButton).toBe(expectedFirstButton);
  expect(actualSecondButton).toBe(expectedSecondButton);
  expect(actualThirdButton).toBe(expectedThirdButton);
  expect(actualFourthButton).toBe(expectedFourthButton);
});

it("shouldRenderPythonRow", () => {
  render(<TableRow lang="python" />, container);

  const actualHeader = container.getElementsByTagName("TH")[0].textContent;
  const expectedHeader = "Python";

  const actualFirstButton = container.getElementsByTagName("BUTTON")[0].textContent;
  const expectedFirstButton = "На 3/4 без ООП";

  const actualSecondButton = container.getElementsByTagName("BUTTON")[1].textContent;
  const expectedSecondButton = "На 3/4 с ООП";

  const actualThirdButton = container.getElementsByTagName("BUTTON")[2].textContent;
  const expectedThirdButton = "На 5 без ООП";

  const actualFourthButton = container.getElementsByTagName("BUTTON")[3].textContent;
  const expectedFourthButton = "На 5 с ООП";

  expect(actualHeader).toBe(expectedHeader);
  expect(actualFirstButton).toBe(expectedFirstButton);
  expect(actualSecondButton).toBe(expectedSecondButton);
  expect(actualThirdButton).toBe(expectedThirdButton);
  expect(actualFourthButton).toBe(expectedFourthButton);
});

it("shouldRenderJavaRow", () => {
  render(<TableRow lang="java" />, container);

  const actualHeader = container.getElementsByTagName("TH")[0].textContent;
  const expectedHeader = "Java";

  const actualFirstButton = container.getElementsByTagName("BUTTON")[0].textContent;
  const expectedFirstButton = "На 3/4 с ООП";

  const actualSecondButton = container.getElementsByTagName("BUTTON")[1].textContent;
  const expectedSecondButton = "На 5 с ООП";

  expect(actualHeader).toBe(expectedHeader);
  expect(actualFirstButton).toBe(expectedFirstButton);
  expect(actualSecondButton).toBe(expectedSecondButton);
});