import TableQuery from "./TableQuery";

const TableRow = props => {
  const PATH = "/year/2020/autumn/actions-overview?";
  const FILE_WITH_OOP = "На 5 с ООП";
  const FILE_NO_OOP = "На 5 без ООП";
  const MANUAL_WITH_OOP = "На 3/4 с ООП";
  const MANUAL_NO_OOP = "На 3/4 без ООП";

  const RenderOnlyOop = () => {
    return (
      <tr>
        <th scope="row">{props.lang.charAt(0).toUpperCase() + props.lang.slice(1)}</th>
        <td>
          <TableQuery
            path={PATH}
            var="1"
            lang={props.lang}
            fileParam="false"
            oopParam="true"
            text={MANUAL_WITH_OOP}
          />
          <TableQuery
            path={PATH}
            var="1"
            lang={props.lang}
            fileParam="true"
            oopParam="true"
            text={FILE_WITH_OOP}
          />
        </td>
        <td>
          <TableQuery
            path={PATH}
            var="2"
            lang={props.lang}
            fileParam="false"
            oopParam="true"
            text={MANUAL_WITH_OOP}
          />
          <TableQuery
            path={PATH}
            var="2"
            lang={props.lang}
            fileParam="true"
            oopParam="true"
            text={FILE_WITH_OOP}
          />
        </td>
        <td>
          <TableQuery
            path={PATH}
            var="3"
            lang={props.lang}
            fileParam="false"
            oopParam="true"
            text={MANUAL_WITH_OOP}
          />
          <TableQuery
            path={PATH}
            var="3"
            lang={props.lang}
            fileParam="true"
            oopParam="true"
            text={FILE_WITH_OOP}
          />
        </td>
      </tr>
    );
  }

  const RenderUsual = () => {
    return (
      <tr>
        <th scope="row">{props.lang.charAt(0).toUpperCase() + props.lang.slice(1)}</th>
        <td>
          <TableQuery
            path={PATH}
            var="1"
            lang={props.lang}
            fileParam="false"
            oopParam="false"
            text={MANUAL_NO_OOP}
          />
          <TableQuery
            path={PATH}
            var="1"
            lang={props.lang}
            fileParam="false"
            oopParam="true"
            text={MANUAL_WITH_OOP}
          />
          <TableQuery
            path={PATH}
            var="1"
            lang={props.lang}
            fileParam="true"
            oopParam="false"
            text={FILE_NO_OOP}
          />
          <TableQuery
            path={PATH}
            var="1"
            lang={props.lang}
            fileParam="true"
            oopParam="true"
            text={FILE_WITH_OOP}
          />
        </td>
        <td>
          <TableQuery
            path={PATH}
            var="2"
            lang={props.lang}
            fileParam="false"
            oopParam="false"
            text={MANUAL_NO_OOP}
          />
          <TableQuery
            path={PATH}
            var="2"
            lang={props.lang}
            fileParam="false"
            oopParam="true"
            text={MANUAL_WITH_OOP}
          />
          <TableQuery
            path={PATH}
            var="2"
            lang={props.lang}
            fileParam="true"
            oopParam="false"
            text={FILE_NO_OOP}
          />
          <TableQuery
            path={PATH}
            var="2"
            lang={props.lang}
            fileParam="true"
            oopParam="true"
            text={FILE_WITH_OOP}
          />
        </td>
        <td>
          <TableQuery
            path={PATH}
            var="3"
            lang={props.lang}
            fileParam="false"
            oopParam="false"
            text={MANUAL_NO_OOP}
          />
          <TableQuery
            path={PATH}
            var="3"
            lang={props.lang}
            fileParam="false"
            oopParam="true"
            text={MANUAL_WITH_OOP}
          />
          <TableQuery
            path={PATH}
            var="3"
            lang={props.lang}
            fileParam="true"
            oopParam="false"
            text={FILE_NO_OOP}
          />
          <TableQuery
            path={PATH}
            var="3"
            lang={props.lang}
            fileParam="true"
            oopParam="true"
            text={FILE_WITH_OOP}
          />
        </td>
      </tr>
    );
  }

  const RenderRow = () => {
    if (props.lang === "java") {
      return <RenderOnlyOop />
    } else {
      return <RenderUsual />
    }
  }

  return <RenderRow />
}

export default TableRow;