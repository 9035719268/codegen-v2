const FileNameView = props => {
  return (
    <div>Название файла:
      <span className="inf">{props.fileName}</span>
    </div>
  );
}

export default FileNameView;