import { render, unmountComponentAtNode } from "react-dom";
import GpsTimeView from "./GpsTimeView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderGpsTimeView", () => {
  const gpsTime = {
    gpsTime: ["1", "2", "3", "4", "5"]
  };
  render(<GpsTimeView gpsTime={gpsTime} />, container);

  const actual = container.textContent;
  const expected = "Массив времен GPS:1, 2, 3, 4, 5, ";
  expect(actual).toBe(expected);
});