import { render, unmountComponentAtNode } from "react-dom";
import TecValuesView from "./TecValuesView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderTecValuesView", () => {
  const tecValues = {
    tecValues: ["1", "2", "3", "4", "5"]
  };

  render(
    <TecValuesView
      pointName="A1"
      fileType="прогнозного"
      tecValues={tecValues}
    />, container
  );

  const actual = container.textContent;
  const expected = "Величина TEC точки A1 прогнозного файла:1, 2, 3, 4, 5, ";
  expect(actual).toBe(expected);
});