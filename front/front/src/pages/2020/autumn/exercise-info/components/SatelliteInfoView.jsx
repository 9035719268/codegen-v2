const SatelliteInfoView = props => {
  return (
    <div>Номер {props.order} спутника:
      <span className="inf">{props.number}</span>
    </div>
  );
}

export default SatelliteInfoView;