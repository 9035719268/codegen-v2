const InputVar3DataView = props => {
  return (
    <div>
      <div>Широта города:
        <span className="inf">{props.lat}</span>
      </div>
      <div>Долгота города:
        <span className="inf">{props.lon}</span>
      </div>
      <div>Меньшая широта точек для интерполяции:
        <span className="inf">{props.lat1}</span>
      </div>
      <div>Большая широта точек для интерполяции:
        <span className="inf">{props.lat2}</span>
      </div>
      <div>Меньшая долгота точек для интерполяции:
        <span className="inf">{props.lon1}</span>
      </div>
      <div>Большая долгота точек для интерполяции:
        <span className="inf">{props.lon2}</span>
      </div>
      <div>Номер спутника для сбора времени GPS:
        <span className="inf">{props.satelliteNumber}</span>
      </div>
    </div>
  );
}

export default InputVar3DataView;