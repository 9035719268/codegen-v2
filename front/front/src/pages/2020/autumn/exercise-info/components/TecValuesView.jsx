const TecValuesView = props => {
  let tecObject = props.tecValues;
  let tecValues = null;
  if (typeof tecObject !== "undefined") {
    tecValues = tecObject.tecValues.map(tecValue => tecValue + ", ");
  }

  return (
    <div>Величина TEC точки {props.pointName} {props.fileType} файла:
      <span className="inf">{tecValues}</span>
    </div>
  );
}

export default TecValuesView;