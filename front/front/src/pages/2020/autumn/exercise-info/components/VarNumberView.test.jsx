import { render, unmountComponentAtNode } from "react-dom";
import VarNumberView from "./VarNumberView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderVarNumberView", () => {
  render(<VarNumberView varNumber="3" />, container);

  const actual = container.textContent;
  const expected = "Номер варианта:3";
  expect(actual).toBe(expected);
});