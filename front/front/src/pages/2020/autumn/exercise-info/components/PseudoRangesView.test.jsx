import { render, unmountComponentAtNode } from "react-dom";
import PseudoRangesView from "./PseudoRangesView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderP1View", () => {
  const p1 = ["1", "2", "3", "4", "5"];
  const p2 = ["6", "7", "8", "9", "10"];
  render(
    <PseudoRangesView
      order="первого"
      p1={p1}
      p2={p2}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[0].textContent;
  const expected = "P1 первого спутника:1, 2, 3, 4, 5, ";
  expect(actual).toBe(expected);
});

it("shouldRenderP2View", () => {
  const p1 = ["1", "2", "3", "4", "5"];
  const p2 = ["6", "7", "8", "9", "10"];
  render(
    <PseudoRangesView
      order="второго"
      p1={p1}
      p2={p2}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[1].textContent;
  const expected = "P2 второго спутника:6, 7, 8, 9, 10, ";
  expect(actual).toBe(expected);
});