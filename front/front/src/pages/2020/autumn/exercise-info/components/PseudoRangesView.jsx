import getValuesDividedByComma from "../../service/getValuesDividedByComma";

const PseudoRangesView = props => {
  const p1 = getValuesDividedByComma(props.p1);
  const p2 = getValuesDividedByComma(props.p2);

  return (
    <div>
      <div className="groupval">P1 {props.order} спутника:
        <div className="inf">{p1}</div>
      </div>
      <div className="groupval">P2 {props.order} спутника:
        <div className="inf">{p2}</div>
      </div>
    </div>
  );
}

export default PseudoRangesView;