import { render, unmountComponentAtNode } from "react-dom";
import InputVar3DataView from "./InputVar3DataView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderInputLatView", () => {
  render(
    <InputVar3DataView
      lat="55"
      lon="162"
      lat1="52.5"
      lat2="57.5"
      lon1="160"
      lon2="165"
      satelliteNumber="7"
    />, container
  );

  const actual = container.getElementsByTagName("DIV")[1].textContent;
  const expected = "Широта города:55";
  expect(actual).toBe(expected);
});

it("shouldRenderInputLonView", () => {
  render(
    <InputVar3DataView
      lat="55"
      lon="162"
      lat1="52.5"
      lat2="57.5"
      lon1="160"
      lon2="165"
      satelliteNumber="7"
    />, container
  );

  const actual = container.getElementsByTagName("DIV")[1].textContent;
  const expected = "Широта города:55";
  expect(actual).toBe(expected);
});

it("shouldRenderInputLat1View", () => {
  render(
    <InputVar3DataView
      lat="55"
      lon="162"
      lat1="52.5"
      lat2="57.5"
      lon1="160"
      lon2="165"
      satelliteNumber="7"
    />, container
  );

  const actual = container.getElementsByTagName("DIV")[2].textContent;
  const expected = "Долгота города:162";
  expect(actual).toBe(expected);
});

it("shouldRenderInputLat2View", () => {
  render(
    <InputVar3DataView
      lat="55"
      lon="162"
      lat1="52.5"
      lat2="57.5"
      lon1="160"
      lon2="165"
      satelliteNumber="7"
    />, container
  );

  const actual = container.getElementsByTagName("DIV")[3].textContent;
  const expected = "Меньшая широта точек для интерполяции:52.5";
  expect(actual).toBe(expected);
});

it("shouldRenderInputLon1View", () => {
  render(
    <InputVar3DataView
      lat="55"
      lon="162"
      lat1="52.5"
      lat2="57.5"
      lon1="160"
      lon2="165"
      satelliteNumber="7"
    />, container
  );

  const actual = container.getElementsByTagName("DIV")[4].textContent;
  const expected = "Большая широта точек для интерполяции:57.5";
  expect(actual).toBe(expected);
});

it("shouldRenderInputLon2View", () => {
  render(
    <InputVar3DataView
      lat="55"
      lon="162"
      lat1="52.5"
      lat2="57.5"
      lon1="160"
      lon2="165"
      satelliteNumber="7"
    />, container
  );

  const actual = container.getElementsByTagName("DIV")[5].textContent;
  const expected = "Меньшая долгота точек для интерполяции:160";
  expect(actual).toBe(expected);
});

it("shouldRenderInputSatelliteNumberView", () => {
  render(
    <InputVar3DataView
      lat="55"
      lon="162"
      lat1="52.5"
      lat2="57.5"
      lon1="160"
      lon2="165"
      satelliteNumber="7"
    />, container
  );

  const actual = container.getElementsByTagName("DIV")[6].textContent;
  const expected = "Большая долгота точек для интерполяции:165";
  expect(actual).toBe(expected);
});