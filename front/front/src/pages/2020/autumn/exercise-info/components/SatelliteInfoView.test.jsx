import { render, unmountComponentAtNode } from "react-dom";
import SatelliteInfoView from "./SatelliteInfoView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderSatelliteInfoView", () => {
  render(<SatelliteInfoView order="первого" number="7" />, container);

  const actual = container.textContent;
  const expected = "Номер первого спутника:7";
  expect(actual).toBe(expected);
});
