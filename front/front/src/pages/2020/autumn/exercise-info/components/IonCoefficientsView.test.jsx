import { render, unmountComponentAtNode } from "react-dom";
import IonCoefficientsView from "./IonCoefficientsView";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderIonCoefficientsView", () => {
  const coefficients = {
    coefficients: ["1", "2", "3", "4", "5"]
  };
  render(
    <IonCoefficientsView
      coefficientsName="альфа"
      coefficients={coefficients}
    />, container
  );

  const actual = container.textContent;
  const expected = "Массив альфа-коэффициентов:1, 2, 3, 4, 5, ";
  expect(actual).toBe(expected);
});