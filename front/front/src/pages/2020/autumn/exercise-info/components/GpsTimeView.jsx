import getValuesDividedByComma from "../../service/getValuesDividedByComma";

const GpsTimeView = props => {
  const gpsTime = getValuesDividedByComma(props.gpsTime.gpsTime);

  return (
    <div>Массив времен GPS:
      <span className="inf">{gpsTime}</span>
    </div>
  );
}

export default GpsTimeView;