import { render, unmountComponentAtNode } from "react-dom";
import Var3ExerciseInfo from "./Var3ExerciseInfo";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderExerciseInfoTitle", () => {
  const componentsData = {
    gpsTime: {
      gpsTime: ["1", "2", "3", "4", "5"]
    },
    alpha: {
      coefficients: ["6", "7", "8", "9", "10"]
    },
    beta: {
      coefficients: ["11", "12", "13", "14", "15"]
    },
    forecastValues: [{
      tecValues: ["16", "17", "18", "19", "20"]
    }, {
      tecValues: ["21", "22", "23", "24", "25"]
    }, {
      tecValues: ["26", "27", "28", "29", "30"]
    }, {
      tecValues: ["31", "32", "33", "34", "35"]
    }
    ],
    preciseValues: [{
      tecValues: ["36", "37", "38", "39", "40"]
    }, {
      tecValues: ["41", "42", "43", "44", "45"]
    }, {
      tecValues: ["46", "47", "48", "49", "50"]
    }, {
      tecValues: ["51", "52", "53", "54", "55"]
    }]
  };

  const inputData = {
    lat: 55,
    lon: 160,
    lat1: 52.5,
    lat2: 57.5,
    lon1: 160,
    lon2: 165,
    satelliteNumber: 7
  };

  render(
    <Var3ExerciseInfo
      componentsData={componentsData}
      inputData={inputData}
    />, container
  );

  const actual = container.getElementsByClassName("ttl")[0].textContent;
  const expected = "Дополнительная информация";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoVarNumber", () => {
  const componentsData = {
    gpsTime: {
      gpsTime: ["1", "2", "3", "4", "5"]
    },
    alpha: {
      coefficients: ["6", "7", "8", "9", "10"]
    },
    beta: {
      coefficients: ["11", "12", "13", "14", "15"]
    },
    forecastValues: [{
      tecValues: ["16", "17", "18", "19", "20"]
    }, {
      tecValues: ["21", "22", "23", "24", "25"]
    }, {
      tecValues: ["26", "27", "28", "29", "30"]
    }, {
      tecValues: ["31", "32", "33", "34", "35"]
    }
    ],
    preciseValues: [{
      tecValues: ["36", "37", "38", "39", "40"]
    }, {
      tecValues: ["41", "42", "43", "44", "45"]
    }, {
      tecValues: ["46", "47", "48", "49", "50"]
    }, {
      tecValues: ["51", "52", "53", "54", "55"]
    }]
  };

  const inputData = {
    lat: 55,
    lon: 160,
    lat1: 52.5,
    lat2: 57.5,
    lon1: 160,
    lon2: 165,
    satelliteNumber: 7
  };

  render(
    <Var3ExerciseInfo
      componentsData={componentsData}
      inputData={inputData}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[0].textContent;
  const expected = "Номер варианта:3";

  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoInputDataLines", () => {
  const componentsData = {
    gpsTime: {
      gpsTime: ["1", "2", "3", "4", "5"]
    },
    alpha: {
      coefficients: ["6", "7", "8", "9", "10"]
    },
    beta: {
      coefficients: ["11", "12", "13", "14", "15"]
    },
    forecastValues: [{
      tecValues: ["16", "17", "18", "19", "20"]
    }, {
      tecValues: ["21", "22", "23", "24", "25"]
    }, {
      tecValues: ["26", "27", "28", "29", "30"]
    }, {
      tecValues: ["31", "32", "33", "34", "35"]
    }
    ],
    preciseValues: [{
      tecValues: ["36", "37", "38", "39", "40"]
    }, {
      tecValues: ["41", "42", "43", "44", "45"]
    }, {
      tecValues: ["46", "47", "48", "49", "50"]
    }, {
      tecValues: ["51", "52", "53", "54", "55"]
    }]
  };

  const inputData = {
    lat: 55,
    lon: 160,
    lat1: 52.5,
    lat2: 57.5,
    lon1: 160,
    lon2: 165,
    satelliteNumber: 7
  };

  render(
    <Var3ExerciseInfo
      componentsData={componentsData}
      inputData={inputData}
    />, container
  );

  const inputDataLines = container.getElementsByClassName("groupval")[1];

  const actualLatLine = inputDataLines.getElementsByTagName("DIV")[1].textContent;
  const expectedLatLine = "Широта города:55";

  const actualLonLine = inputDataLines.getElementsByTagName("DIV")[2].textContent;
  const expectedLonLine = "Долгота города:160";

  const actualLat1Line = inputDataLines.getElementsByTagName("DIV")[3].textContent;
  const expectedLat1Line = "Меньшая широта точек для интерполяции:52.5";

  const actualLat2Line = inputDataLines.getElementsByTagName("DIV")[4].textContent;
  const expectedLat2Line = "Большая широта точек для интерполяции:57.5";

  const actualLon1Line = inputDataLines.getElementsByTagName("DIV")[5].textContent;
  const expectedLon1Line = "Меньшая долгота точек для интерполяции:160";

  const actualLon2Line = inputDataLines.getElementsByTagName("DIV")[6].textContent;
  const expectedLon2Line = "Большая долгота точек для интерполяции:165";

  const actualSatelliteNumberLine = inputDataLines.getElementsByTagName("DIV")[7].textContent;
  const expectedSatelliteNumberLine = "Номер спутника для сбора времени GPS:7";

  expect(actualLatLine).toBe(expectedLatLine);
  expect(actualLonLine).toBe(expectedLonLine);
  expect(actualLat1Line).toBe(expectedLat1Line);
  expect(actualLat2Line).toBe(expectedLat2Line);
  expect(actualLon1Line).toBe(expectedLon1Line);
  expect(actualLon2Line).toBe(expectedLon2Line);
  expect(actualSatelliteNumberLine).toBe(expectedSatelliteNumberLine);
});

it("shouldRenderExerciseInfoGpsTime", () => {
  const componentsData = {
    gpsTime: {
      gpsTime: ["1", "2", "3", "4", "5"]
    },
    alpha: {
      coefficients: ["6", "7", "8", "9", "10"]
    },
    beta: {
      coefficients: ["11", "12", "13", "14", "15"]
    },
    forecastValues: [{
      tecValues: ["16", "17", "18", "19", "20"]
    }, {
      tecValues: ["21", "22", "23", "24", "25"]
    }, {
      tecValues: ["26", "27", "28", "29", "30"]
    }, {
      tecValues: ["31", "32", "33", "34", "35"]
    }
    ],
    preciseValues: [{
      tecValues: ["36", "37", "38", "39", "40"]
    }, {
      tecValues: ["41", "42", "43", "44", "45"]
    }, {
      tecValues: ["46", "47", "48", "49", "50"]
    }, {
      tecValues: ["51", "52", "53", "54", "55"]
    }]
  };

  const inputData = {
    lat: 55,
    lon: 160,
    lat1: 52.5,
    lat2: 57.5,
    lon1: 160,
    lon2: 165,
    satelliteNumber: 7
  };

  render(
    <Var3ExerciseInfo
      componentsData={componentsData}
      inputData={inputData}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[2].textContent;
  const expected = "Массив времен GPS:1, 2, 3, 4, 5, ";

  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoIonCoefficients", () => {
  const componentsData = {
    gpsTime: {
      gpsTime: ["1", "2", "3", "4", "5"]
    },
    alpha: {
      coefficients: ["6", "7", "8", "9", "10"]
    },
    beta: {
      coefficients: ["11", "12", "13", "14", "15"]
    },
    forecastValues: [{
      tecValues: ["16", "17", "18", "19", "20"]
    }, {
      tecValues: ["21", "22", "23", "24", "25"]
    }, {
      tecValues: ["26", "27", "28", "29", "30"]
    }, {
      tecValues: ["31", "32", "33", "34", "35"]
    }
    ],
    preciseValues: [{
      tecValues: ["36", "37", "38", "39", "40"]
    }, {
      tecValues: ["41", "42", "43", "44", "45"]
    }, {
      tecValues: ["46", "47", "48", "49", "50"]
    }, {
      tecValues: ["51", "52", "53", "54", "55"]
    }]
  };

  const inputData = {
    lat: 55,
    lon: 160,
    lat1: 52.5,
    lat2: 57.5,
    lon1: 160,
    lon2: 165,
    satelliteNumber: 7
  };

  render(
    <Var3ExerciseInfo
      componentsData={componentsData}
      inputData={inputData}
    />, container
  );

  const coefficientsGroup = container.getElementsByClassName("groupval")[3];

  const actualAlpha = coefficientsGroup.getElementsByTagName("DIV")[0].textContent;
  const expectedAlpha = "Массив альфа-коэффициентов:6, 7, 8, 9, 10, ";

  const actualBeta = coefficientsGroup.getElementsByTagName("DIV")[1].textContent;
  const expectedBeta = "Массив бета-коэффициентов:11, 12, 13, 14, 15, ";

  expect(actualAlpha).toBe(expectedAlpha);
  expect(actualBeta).toBe(expectedBeta);
});

it("shouldRenderExerciseInfoTecValues", () => {
  const componentsData = {
    gpsTime: {
      gpsTime: ["1", "2", "3", "4", "5"]
    },
    alpha: {
      coefficients: ["6", "7", "8", "9", "10"]
    },
    beta: {
      coefficients: ["11", "12", "13", "14", "15"]
    },
    forecastValues: [{
      tecValues: ["16", "17", "18", "19", "20"]
    }, {
      tecValues: ["21", "22", "23", "24", "25"]
    }, {
      tecValues: ["26", "27", "28", "29", "30"]
    }, {
      tecValues: ["31", "32", "33", "34", "35"]
    }
    ],
    preciseValues: [{
      tecValues: ["36", "37", "38", "39", "40"]
    }, {
      tecValues: ["41", "42", "43", "44", "45"]
    }, {
      tecValues: ["46", "47", "48", "49", "50"]
    }, {
      tecValues: ["51", "52", "53", "54", "55"]
    }]
  };

  const inputData = {
    lat: 55,
    lon: 160,
    lat1: 52.5,
    lat2: 57.5,
    lon1: 160,
    lon2: 165,
    satelliteNumber: 7
  };

  render(
    <Var3ExerciseInfo
      componentsData={componentsData}
      inputData={inputData}
    />, container
  );

  const tecValuesGroup = container.getElementsByClassName("groupval")[4];

  const actualForecastA1 = tecValuesGroup.getElementsByTagName("DIV")[0].textContent;
  const expectedForecastA1 = "Величина TEC точки A1 прогнозного файла:16, 17, 18, 19, 20, ";

  const actualForecastA2 = tecValuesGroup.getElementsByTagName("DIV")[1].textContent;
  const expectedForecastA2 = "Величина TEC точки A2 прогнозного файла:21, 22, 23, 24, 25, ";

  const actualForecastA3 = tecValuesGroup.getElementsByTagName("DIV")[2].textContent;
  const expectedForecastA3 = "Величина TEC точки A3 прогнозного файла:26, 27, 28, 29, 30, ";

  const actualForecastA4 = tecValuesGroup.getElementsByTagName("DIV")[3].textContent;
  const expectedForecastA4 = "Величина TEC точки A4 прогнозного файла:31, 32, 33, 34, 35, ";

  const actualPreciseA1 = tecValuesGroup.getElementsByTagName("DIV")[4].textContent;
  const expectedPreciseA1 = "Величина TEC точки A1 точного файла:36, 37, 38, 39, 40, ";

  const actualPreciseA2 = tecValuesGroup.getElementsByTagName("DIV")[5].textContent;
  const expectedPreciseA2 = "Величина TEC точки A2 точного файла:41, 42, 43, 44, 45, ";

  const actualPreciseA3 = tecValuesGroup.getElementsByTagName("DIV")[6].textContent;
  const expectedPreciseA3 = "Величина TEC точки A3 точного файла:46, 47, 48, 49, 50, ";

  const actualPreciseA4 = tecValuesGroup.getElementsByTagName("DIV")[7].textContent;
  const expectedPreciseA4 = "Величина TEC точки A4 точного файла:51, 52, 53, 54, 55, ";

  expect(actualForecastA1).toBe(expectedForecastA1);
  expect(actualForecastA2).toBe(expectedForecastA2);
  expect(actualForecastA3).toBe(expectedForecastA3);
  expect(actualForecastA4).toBe(expectedForecastA4);
  expect(actualPreciseA1).toBe(expectedPreciseA1);
  expect(actualPreciseA2).toBe(expectedPreciseA2);
  expect(actualPreciseA3).toBe(expectedPreciseA3);
  expect(actualPreciseA4).toBe(expectedPreciseA4);
});