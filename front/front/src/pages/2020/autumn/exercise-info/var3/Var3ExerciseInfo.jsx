import "../../../../../index.css";
import VarNumberView from "../components/VarNumberView";
import InputVar3DataView from "../components/InputVar3DataView";
import GpsTimeView from "../components/GpsTimeView";
import IonCoefficientsView from "../components/IonCoefficientsView";
import TecValuesView from "../components/TecValuesView";

const Var3ExerciseInfo = props => {
  const [
    gpsTime, alpha, beta,
    forecastValues, preciseValues
  ] = Object.values(props.componentsData);

  const [
    lat, lon, lat1, lat2, lon1, lon2, satelliteNumber
  ] = Object.values(props.inputData);

  return (
    <div>
      <div className="ttl">Дополнительная информация</div>
      <div className="groupval">
        <VarNumberView varNumber="3" />
      </div>
      <div className="groupval">
        <InputVar3DataView
          lat={lat} lon={lon}
          lat1={lat1} lat2={lat2}
          lon1={lon1} lon2={lon2}
          satelliteNumber={satelliteNumber}
        />
      </div>
      <div className="groupval">
        <GpsTimeView gpsTime={gpsTime} />
      </div>
      <div className="groupval">
        <IonCoefficientsView coefficientsName="альфа" coefficients={alpha} />
        <IonCoefficientsView coefficientsName="бета" coefficients={beta} />
      </div>
      <div className="groupval">
        <TecValuesView pointName="A1" fileType="прогнозного" tecValues={forecastValues[0]} />
        <TecValuesView pointName="A2" fileType="прогнозного" tecValues={forecastValues[1]} />
        <TecValuesView pointName="A3" fileType="прогнозного" tecValues={forecastValues[2]} />
        <TecValuesView pointName="A4" fileType="прогнозного" tecValues={forecastValues[3]} />
        <TecValuesView pointName="A1" fileType="точного" tecValues={preciseValues[0]} />
        <TecValuesView pointName="A2" fileType="точного" tecValues={preciseValues[1]} />
        <TecValuesView pointName="A3" fileType="точного" tecValues={preciseValues[2]} />
        <TecValuesView pointName="A4" fileType="точного" tecValues={preciseValues[3]} />
      </div>
    </div>
  );
}

export default Var3ExerciseInfo;