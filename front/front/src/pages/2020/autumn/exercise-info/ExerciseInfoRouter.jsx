import { extractVarNumber } from "../service/queryParamsExtraction";
import FetchVar1ExerciseInfo from "./var1/FetchVar1ExerciseInfo";
import FetchVar2ExerciseInfo from "./var2/FetchVar2ExerciseInfo";
import FetchVar3ExerciseInfo from "./var3/FetchVar3ExerciseInfo";

const ExerciseInfoRouter = props => {
  const routerSearch = props.location.search;
  const varNumber = extractVarNumber(routerSearch);
  if (varNumber === "1")
    return <FetchVar1ExerciseInfo routerSearch={routerSearch} />
  else if (varNumber === "2")
    return <FetchVar2ExerciseInfo routerSearch={routerSearch} />
  else if (varNumber === "3")
    return <FetchVar3ExerciseInfo routerSearch={routerSearch} />
}

export default ExerciseInfoRouter;