import axios from "axios";

const getVar2SatellitesData = fileName => {
  const satellitesData = axios.get("http://localhost:8080/year/2020/autumn/exercise-info/var2", {
    params: {
      filename: fileName
    }
  }).then(response => {
    const satellite1 = response.data[0];
    const satellite2 = response.data[1];
    const satellite3 = response.data[2];
    return {satellite1, satellite2, satellite3}
  });

  return satellitesData;
}

export default getVar2SatellitesData;