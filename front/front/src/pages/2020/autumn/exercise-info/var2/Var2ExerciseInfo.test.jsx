import { render, unmountComponentAtNode } from "react-dom";
import Var2ExerciseInfo from "./Var2ExerciseInfo";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderExerciseInfoTitle", () => {
  const satellite1Data = {
    number: 7,
    elevation: ["1", "2", "3", "4", "5"]
  };

  const satellite2Data = {
    number: 14,
    elevation: ["6", "7", "8", "9", "10"]
  };

  const satellite3Data = {
    number: 21,
    elevation: ["11", "12", "13", "14", "15"]
  };

  render(
    <Var2ExerciseInfo
      varNumber="2"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("ttl")[0].textContent;
  const expected = "Дополнительная информация";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoInputDataLines", () => {
  const satellite1Data = {
    number: 7,
    elevation: ["1", "2", "3", "4", "5"]
  };

  const satellite2Data = {
    number: 14,
    elevation: ["6", "7", "8", "9", "10"]
  };

  const satellite3Data = {
    number: 21,
    elevation: ["11", "12", "13", "14", "15"]
  };

  render(
    <Var2ExerciseInfo
      varNumber="2"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const inputData = container.getElementsByClassName("groupval")[0];

  const actualVarNumber = inputData.getElementsByTagName("DIV")[0].textContent;
  const expectedVarNumber = "Номер варианта:2";

  const actualFileName = inputData.getElementsByTagName("DIV")[1].textContent;
  const expectedFileName = "Название файла:POTS_6hours.dat";

  expect(actualVarNumber).toBe(expectedVarNumber);
  expect(actualFileName).toBe(expectedFileName);
});

it("shouldRenderExerciseInfoSatelliteNumbersLines", () => {
  const satellite1Data = {
    number: 7,
    elevation: ["1", "2", "3", "4", "5"]
  };

  const satellite2Data = {
    number: 14,
    elevation: ["6", "7", "8", "9", "10"]
  };

  const satellite3Data = {
    number: 21,
    elevation: ["11", "12", "13", "14", "15"]
  };

  render(
    <Var2ExerciseInfo
      varNumber="2"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const satelliteNumbersGroup = container.getElementsByClassName("groupval")[1];

  const actualSatellite1NumberLine = satelliteNumbersGroup.getElementsByTagName("DIV")[0].textContent;
  const expectedSatellite1NumberLine = "Номер первого спутника:7";

  const actualSatellite2NumberLine = satelliteNumbersGroup.getElementsByTagName("DIV")[1].textContent;
  const expectedSatellite2NumberLine = "Номер второго спутника:14";

  const actualSatellite3NumberLine = satelliteNumbersGroup.getElementsByTagName("DIV")[2].textContent;
  const expectedSatellite3NumberLine = "Номер третьего спутника:21";

  expect(actualSatellite1NumberLine).toBe(expectedSatellite1NumberLine);
  expect(actualSatellite2NumberLine).toBe(expectedSatellite2NumberLine);
  expect(actualSatellite3NumberLine).toBe(expectedSatellite3NumberLine);
});

it("shouldRenderExerciseInfoSatellite1Elevation", () => {
  const satellite1Data = {
    number: 7,
    elevation: ["1", "2", "3", "4", "5"]
  };

  const satellite2Data = {
    number: 14,
    elevation: ["6", "7", "8", "9", "10"]
  };

  const satellite3Data = {
    number: 21,
    elevation: ["11", "12", "13", "14", "15"]
  };

  render(
    <Var2ExerciseInfo
      varNumber="2"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[2].textContent;
  const expected = "Углы возвышения первого спутника:1, 2, 3, 4, 5, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite2Elevation", () => {
  const satellite1Data = {
    number: 7,
    elevation: ["1", "2", "3", "4", "5"]
  };

  const satellite2Data = {
    number: 14,
    elevation: ["6", "7", "8", "9", "10"]
  };

  const satellite3Data = {
    number: 21,
    elevation: ["11", "12", "13", "14", "15"]
  };

  render(
    <Var2ExerciseInfo
      varNumber="2"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[3].textContent;
  const expected = "Углы возвышения второго спутника:6, 7, 8, 9, 10, ";
  expect(actual).toBe(expected);
});

it("shouldRenderExerciseInfoSatellite3Elevation", () => {
  const satellite1Data = {
    number: 7,
    elevation: ["1", "2", "3", "4", "5"]
  };

  const satellite2Data = {
    number: 14,
    elevation: ["6", "7", "8", "9", "10"]
  };

  const satellite3Data = {
    number: 21,
    elevation: ["11", "12", "13", "14", "15"]
  };

  render(
    <Var2ExerciseInfo
      varNumber="2"
      fileName="POTS_6hours.dat"
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />, container
  );

  const actual = container.getElementsByClassName("groupval")[4].textContent;
  const expected = "Углы возвышения третьего спутника:11, 12, 13, 14, 15, ";
  expect(actual).toBe(expected);
});