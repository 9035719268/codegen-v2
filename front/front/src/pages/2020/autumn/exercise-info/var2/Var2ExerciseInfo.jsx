import "../../../../../index.css";
import VarNumberView from "../components/VarNumberView";
import SatelliteInfoView from "../components/SatelliteInfoView";
import ElevationAnglesView from "../components/ElevationAnglesView";
import FileNameView from "../components/FileNameView";

const Var2ExerciseInfo = props => {
  const [satellite1Number, satellite1Elevation] = Object.values(props.satellite1Data);
  const [satellite2Number, satellite2Elevation] = Object.values(props.satellite2Data);
  const [satellite3Number, satellite3Elevation] = Object.values(props.satellite3Data);

  return (
    <div>
      <div className="ttl">Дополнительная информация</div>
      <div className="groupval">
        <VarNumberView varNumber={props.varNumber} />
        <FileNameView fileName={props.fileName} />
      </div>
      <div className="groupval">
        <SatelliteInfoView order="первого" number={satellite1Number} />
        <SatelliteInfoView order="второго" number={satellite2Number} />
        <SatelliteInfoView order="третьего" number={satellite3Number} />
      </div>
      <ElevationAnglesView
        order="первого"
        elevation={satellite1Elevation}
      />
      <ElevationAnglesView
        order="второго"
        elevation={satellite2Elevation}
      />
      <ElevationAnglesView
        order="третьего"
        elevation={satellite3Elevation}
      />
    </div>
  );
}

export default Var2ExerciseInfo;