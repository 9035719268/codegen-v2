const JavaExample = props => {
  return (
    <span>
      <a href={props.path + "MainExample.java"} download="MainExample.java">
        <button type="submit">Загрузить MainExample.java</button>
      </a>
      <a href={props.path + "GraphDrawerExample.java"} download="GraphDrawerExample.java">
        <button type="submit">Загрузить GraphDrawerExample.java</button>
      </a>
    </span>
  );
}

export default JavaExample;