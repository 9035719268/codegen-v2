import { render, unmountComponentAtNode } from "react-dom";
import GetExerciseInfo from "./GetExerciseInfo";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderGetExerciseInfoButton", () => {
  render(<GetExerciseInfo />, container);

  const actual = container.getElementsByTagName("BUTTON")[0].textContent;
  const expected = "Получить информацию по заданию";
  expect(actual).toBe(expected);
});