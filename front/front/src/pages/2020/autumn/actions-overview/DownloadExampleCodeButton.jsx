import JavaExample from "./JavaExample";
import UsualExample from "./UsualExample";

const DownloadExampleCode = props => {
  const PATH = "http://localhost:8080/year/2020/autumn/download/" +
    "var" + props.varNumber + "/" +
    props.lang + "/" +
    props.fileParam + "/" +
    props.oopParam + "/";

  const fileName = props.lang === "python" ? "mainExample.py" : "mainExample.cpp";

  if (props.lang === "java") {
    return <JavaExample path={PATH} />
  } else {
    return <UsualExample path={PATH} fileName={fileName} />
  }
}

export default DownloadExampleCode;