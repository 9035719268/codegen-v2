import JavaCode from "./JavaCode";
import UsualCode from "./UsualCode";

const DownloadCodeButton = props => {
  const PATH = "http://localhost:8080/year/2020/autumn/download/" +
    "var" + props.varNumber + "/" +
    props.lang + "/" +
    props.fileParam + "/" +
    props.oopParam + "/";

  const fileName = props.lang === "python" ? "main.py" : "main.cpp";

  if (props.lang === "java") {
    return <JavaCode path={PATH} />
  } else {
    return <UsualCode path={PATH} fileName={fileName} />
  }
}

export default DownloadCodeButton;