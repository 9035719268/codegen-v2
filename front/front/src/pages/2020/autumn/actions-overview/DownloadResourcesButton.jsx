const DownloadResourcesButton = () => {
  return (
    <a href="http://localhost:8080/year/2020/autumn/download/resources" download="resources.rar">
      <button className="overview" type="submit">Загрузить resources.rar</button>
    </a>
  );
}

export default DownloadResourcesButton;