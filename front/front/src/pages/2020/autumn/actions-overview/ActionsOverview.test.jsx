import { render, unmountComponentAtNode } from "react-dom";
import ActionsOverview from "./ActionsOverview";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldReturnValuesForVar1CppManualNoOop", () => {
  const location = {
    search: "?var=1&lang=cpp&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar1CppManualWithOop", () => {
  const location = {
    search: "?var=1&lang=cpp&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar1CppFileNoOop", () => {
  const location = {
    search: "?var=1&lang=cpp&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar1CppFileWithOop", () => {
  const location = {
    search: "?var=1&lang=cpp&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar1PythonManualNoOop", () => {
  const location = {
    search: "?var=1&lang=python&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar1PythonManualWithOop", () => {
  const location = {
    search: "?var=1&lang=python&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar1PythonFileNoOop", () => {
  const location = {
    search: "?var=1&lang=python&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar1PythonFileWithOop", () => {
  const location = {
    search: "?var=1&lang=python&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar1JavaManualWithOop", () => {
  const location = {
    search: "?var=1&lang=java&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Java, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить Main.java";

  const actualDownloadGraphDrawerButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadGraphDrawerButton = "Загрузить GraphDrawer.java";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[2].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[3].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[4].textContent;
  const expectedDownloadMainExampleButton = "Загрузить MainExample.java";

  const actualDownloadGraphDrawerExampleButton = container.getElementsByTagName("A")[5].textContent;
  const expectedDownloadGraphDrawerExampleButton = "Загрузить GraphDrawerExample.java";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadGraphDrawerButton).toBe(expectedDownloadGraphDrawerButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
  expect(actualDownloadGraphDrawerExampleButton).toBe(expectedDownloadGraphDrawerExampleButton);
});

it("shouldReturnValuesForVar1JavaFileWithOop", () => {
  const location = {
    search: "?var=1&lang=java&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "1 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Java, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить Main.java";

  const actualDownloadGraphDrawerButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadGraphDrawerButton = "Загрузить GraphDrawer.java";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[2].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[3].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[4].textContent;
  const expectedDownloadMainExampleButton = "Загрузить MainExample.java";

  const actualDownloadGraphDrawerExampleButton = container.getElementsByTagName("A")[5].textContent;
  const expectedDownloadGraphDrawerExampleButton = "Загрузить GraphDrawerExample.java";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadGraphDrawerButton).toBe(expectedDownloadGraphDrawerButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
  expect(actualDownloadGraphDrawerExampleButton).toBe(expectedDownloadGraphDrawerExampleButton);
});

it("shouldReturnValuesForVar2CppManualNoOop", () => {
  const location = {
    search: "?var=2&lang=cpp&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar2CppManualWithOop", () => {
  const location = {
    search: "?var=2&lang=cpp&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar2CppFileNoOop", () => {
  const location = {
    search: "?var=2&lang=cpp&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar2CppFileWithOop", () => {
  const location = {
    search: "?var=2&lang=cpp&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar2PythonManualNoOop", () => {
  const location = {
    search: "?var=2&lang=python&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar2PythonManualWithOop", () => {
  const location = {
    search: "?var=2&lang=python&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar2PythonFileNoOop", () => {
  const location = {
    search: "?var=2&lang=python&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar2PythonFileWithOop", () => {
  const location = {
    search: "?var=2&lang=python&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar2JavaManualWithOop", () => {
  const location = {
    search: "?var=2&lang=java&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Java, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить Main.java";

  const actualDownloadGraphDrawerButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadGraphDrawerButton = "Загрузить GraphDrawer.java";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[2].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[3].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[4].textContent;
  const expectedDownloadMainExampleButton = "Загрузить MainExample.java";

  const actualDownloadGraphDrawerExampleButton = container.getElementsByTagName("A")[5].textContent;
  const expectedDownloadGraphDrawerExampleButton = "Загрузить GraphDrawerExample.java";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadGraphDrawerButton).toBe(expectedDownloadGraphDrawerButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
  expect(actualDownloadGraphDrawerExampleButton).toBe(expectedDownloadGraphDrawerExampleButton);
});

it("shouldReturnValuesForVar2JavaFileWithOop", () => {
  const location = {
    search: "?var=2&lang=java&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "2 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Java, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить Main.java";

  const actualDownloadGraphDrawerButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadGraphDrawerButton = "Загрузить GraphDrawer.java";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[2].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[3].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[4].textContent;
  const expectedDownloadMainExampleButton = "Загрузить MainExample.java";

  const actualDownloadGraphDrawerExampleButton = container.getElementsByTagName("A")[5].textContent;
  const expectedDownloadGraphDrawerExampleButton = "Загрузить GraphDrawerExample.java";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadGraphDrawerButton).toBe(expectedDownloadGraphDrawerButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
  expect(actualDownloadGraphDrawerExampleButton).toBe(expectedDownloadGraphDrawerExampleButton);
});

it("shouldReturnValuesForVar3CppManualNoOop", () => {
  const location = {
    search: "?var=3&lang=cpp&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar3CppManualWithOop", () => {
  const location = {
    search: "?var=3&lang=cpp&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar3CppFileNoOop", () => {
  const location = {
    search: "?var=3&lang=cpp&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar3CppFileWithOop", () => {
  const location = {
    search: "?var=3&lang=cpp&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Cpp, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.cpp";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.cpp";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar3PythonManualNoOop", () => {
  const location = {
    search: "?var=3&lang=python&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar3PythonManualWithOop", () => {
  const location = {
    search: "?var=3&lang=python&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar3PythonFileNoOop", () => {
  const location = {
    search: "?var=3&lang=python&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "без ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar3PythonFileWithOop", () => {
  const location = {
    search: "?var=3&lang=python&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Python, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить main.py";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[2].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[3].textContent;
  const expectedDownloadMainExampleButton = "Загрузить mainExample.py";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
});

it("shouldReturnValuesForVar3JavaManualWithOop", () => {
  const location = {
    search: "?var=3&lang=java&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Java, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "без чтения файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить Main.java";

  const actualDownloadGraphDrawerButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadGraphDrawerButton = "Загрузить GraphDrawer.java";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[2].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[3].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[4].textContent;
  const expectedDownloadMainExampleButton = "Загрузить MainExample.java";

  const actualDownloadGraphDrawerExampleButton = container.getElementsByTagName("A")[5].textContent;
  const expectedDownloadGraphDrawerExampleButton = "Загрузить GraphDrawerExample.java";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadGraphDrawerButton).toBe(expectedDownloadGraphDrawerButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
  expect(actualDownloadGraphDrawerExampleButton).toBe(expectedDownloadGraphDrawerExampleButton);
});

it("shouldReturnValuesForVar3JavaFileWithOop", () => {
  const location = {
    search: "?var=3&lang=java&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  const actualTimelapseText = container.getElementsByTagName("P")[0].textContent;
  const expectedTimelapseText = "Осень, 2020 год";

  const actualVarText = container.getElementsByTagName("SPAN")[0].textContent;
  const expectedVarText = "3 вариант, ";

  const actualLangText = container.getElementsByTagName("SPAN")[1].textContent;
  const expectedLangText = "язык программирования: Java, ";

  const actualFileParamText = container.getElementsByTagName("SPAN")[2].textContent;
  const expectedFileParamText = "с чтением файла, ";

  const actualOopParamText = container.getElementsByTagName("SPAN")[3].textContent;
  const expectedOopParamText = "с ООП";

  const actualDownloadMainButton = container.getElementsByTagName("A")[0].textContent;
  const expectedDownloadMainButton = "Загрузить Main.java";

  const actualDownloadGraphDrawerButton = container.getElementsByTagName("A")[1].textContent;
  const expectedDownloadGraphDrawerButton = "Загрузить GraphDrawer.java";

  const actualDownloadResourcesButton = container.getElementsByTagName("A")[2].textContent;
  const expectedDownloadResourcesButton = "Загрузить resources.rar";

  const actualGetExerciseInfoButton = container.getElementsByTagName("A")[3].textContent;
  const expectedGetExerciseInfoButton = "Получить информацию по заданию";

  const actualDownloadMainExampleButton = container.getElementsByTagName("A")[4].textContent;
  const expectedDownloadMainExampleButton = "Загрузить MainExample.java";

  const actualDownloadGraphDrawerExampleButton = container.getElementsByTagName("A")[5].textContent;
  const expectedDownloadGraphDrawerExampleButton = "Загрузить GraphDrawerExample.java";

  expect(actualTimelapseText).toBe(expectedTimelapseText);
  expect(actualVarText).toBe(expectedVarText);
  expect(actualLangText).toBe(expectedLangText);
  expect(actualFileParamText).toBe(expectedFileParamText);
  expect(actualOopParamText).toBe(expectedOopParamText);
  expect(actualDownloadMainButton).toBe(expectedDownloadMainButton);
  expect(actualDownloadGraphDrawerButton).toBe(expectedDownloadGraphDrawerButton);
  expect(actualDownloadResourcesButton).toBe(expectedDownloadResourcesButton);
  expect(actualGetExerciseInfoButton).toBe(expectedGetExerciseInfoButton);
  expect(actualDownloadMainExampleButton).toBe(expectedDownloadMainExampleButton);
  expect(actualDownloadGraphDrawerExampleButton).toBe(expectedDownloadGraphDrawerExampleButton);
});