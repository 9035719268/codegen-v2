const GetExerciseInfo = props => {
  const PATH = "http://localhost:3000/year/2020/autumn/exercise-info-form?var=" + props.varNumber;

  return (
    <a href={PATH}>
      <button className="overview" type="submit">Получить информацию по заданию</button>
    </a>
  );
}

export default GetExerciseInfo;