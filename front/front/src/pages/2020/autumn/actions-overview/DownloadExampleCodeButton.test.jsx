import { render, unmountComponentAtNode } from "react-dom";
import DownloadExampleCodeButton from "./DownloadExampleCodeButton";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppDownloadExampleCodeButton", () => {
  render(<DownloadExampleCodeButton lang="cpp" />, container);

  const actual = container.getElementsByTagName("A")[0].textContent;
  const expected = "Загрузить mainExample.cpp";
  expect(actual).toBe(expected);
});

it("shouldRenderPythonDownloadExampleCodeButton", () => {
  render(<DownloadExampleCodeButton lang="python" />, container);

  const actual = container.getElementsByTagName("A")[0].textContent;
  const expected = "Загрузить mainExample.py";
  expect(actual).toBe(expected);
});

it("shouldRenderJavaDownloadExampleCodeButtons", () => {
  render(<DownloadExampleCodeButton lang="java" />, container);

  const actualMainExample = container.getElementsByTagName("A")[0].textContent;
  const expectedMainExample = "Загрузить MainExample.java";

  const actualGraphDrawerExample = container.getElementsByTagName("A")[1].textContent;
  const expectedGraphDrawerExample = "Загрузить GraphDrawerExample.java";

  expect(actualMainExample).toBe(expectedMainExample);
  expect(actualGraphDrawerExample).toBe(expectedGraphDrawerExample);
});