import {
  extractVarNumber, extractLang, extractFileParam, extractOopParam, extractFileName,
  extractLat, extractLon, extractLat1, extractLat2, extractLon1, extractLon2,
  extractSatelliteNumber
} from "./queryParamsExtraction"

it("shouldGetVarNumber", () => {
  const actual = extractVarNumber("?var=3");
  const expected = "3";
  expect(actual).toBe(expected);
});

it("shouldGetLang", () => {
  const actual = extractLang("?lang=cpp");
  const expected = "cpp";
  expect(actual).toBe(expected);
});

it("shouldGetFile", () => {
  const actual = extractFileParam("?file=true");
  const expected = "file";
  expect(actual).toBe(expected);
});

it("shouldGetManual", () => {
  const actual = extractFileParam("?file=false");
  const expected = "manual";
  expect(actual).toBe(expected);
});

it("shouldGetWithOop", () => {
  const actual = extractOopParam("?oop=true");
  const expected = "withoop";
  expect(actual).toBe(expected);
});

it("shouldGetNoOop", () => {
  const actual = extractOopParam("?oop=false");
  const expected = "nooop";
  expect(actual).toBe(expected);
});

it("shouldGetFileName", () => {
  const actual = extractFileName("?filename=POTS_6hours.dat");
  const expected = "POTS_6hours.dat";
  expect(actual).toBe(expected);
});

it("shouldGetLat", () => {
  const actual = extractLat("?lat=145");
  const expected = "145";
  expect(actual).toBe(expected);
});

it("shouldGetLon", () => {
  const actual = extractLon("?lon=60");
  const expected = "60";
  expect(actual).toBe(expected);
});

it("shouldGetLat1", () => {
  const actual = extractLat1("?lat1=140");
  const expected = "140";
  expect(actual).toBe(expected);
});

it("shouldGetLat2", () => {
  const actual = extractLat2("?lat2=150");
  const expected = "150";
  expect(actual).toBe(expected);
});

it("shouldGetLon1", () => {
  const actual = extractLon1("?lon1=55");
  const expected = "55";
  expect(actual).toBe(expected);
});

it("shouldGetLon2", () => {
  const actual = extractLon2("?lon2=65");
  const expected = "65";
  expect(actual).toBe(expected);
});

it("shouldGetSatelliteNumber", () => {
  const actual = extractSatelliteNumber("?satnum=7");
  const expected = "7";
  expect(actual).toBe(expected);
});

it("shouldGetAllParams", () => {
  const routerSearch = "?var=3" +
    "&lang=python" +
    "&file=false" +
    "&oop=true" +
    "&filename=POTS_6hours.dat" +
    "&lat=130" +
    "&lon=50" +
    "&lat1=125" +
    "&lat2=135" +
    "&lon1=45" +
    "&lon2=55" +
    "&satnum=5";

  const actualVarNumber = extractVarNumber(routerSearch);
  const expectedVarNumber = "3";

  const actualLang = extractLang(routerSearch);
  const expectedLang = "python";

  const actualFileParam = extractFileParam(routerSearch);
  const expectedFileParam = "manual";

  const actualOopParam = extractOopParam(routerSearch);
  const expectedOopParam = "withoop";

  const actualFileName = extractFileName(routerSearch);
  const expectedFileName = "POTS_6hours.dat";

  const actualLat = extractLat(routerSearch);
  const expectedLat = "130";

  const actualLon = extractLon(routerSearch);
  const expectedLon = "50";

  const actualLat1 = extractLat1(routerSearch);
  const expectedLat1 = "125";

  const actualLat2 = extractLat2(routerSearch);
  const expectedLat2 = "135";

  const actualLon1 = extractLon1(routerSearch);
  const expectedLon1 = "45";

  const actualLon2 = extractLon2(routerSearch);
  const expectedLon2 = "55";

  const actualSatelliteNumber = extractSatelliteNumber(routerSearch);
  const expectedSatelliteNumber = "5";

  expect(actualVarNumber).toBe(expectedVarNumber);
  expect(actualLang).toBe(expectedLang);
  expect(actualFileParam).toBe(expectedFileParam);
  expect(actualOopParam).toBe(expectedOopParam);
  expect(actualFileName).toBe(expectedFileName);
  expect(actualLat).toBe(expectedLat);
  expect(actualLon).toBe(expectedLon);
  expect(actualLat1).toBe(expectedLat1);
  expect(actualLat2).toBe(expectedLat2);
  expect(actualLon1).toBe(expectedLon1);
  expect(actualLon2).toBe(expectedLon2);
  expect(actualSatelliteNumber).toBe(expectedSatelliteNumber);
})