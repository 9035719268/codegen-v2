export const extractVarNumber = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const varNumber = searchParams.get("var");
  return varNumber;
}

export const extractLang = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const lang = searchParams.get("lang");
  return lang;
}

export const extractFileParam = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const type = searchParams.get("file") === "true" ? "file" : "manual";
  return type;
}

export const extractOopParam = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const type = searchParams.get("oop") === "true" ? "withoop" : "nooop";
  return type;
}

export const extractFileName = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const fileName = searchParams.get("filename");
  return fileName;
}

export const extractLat = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const lat = searchParams.get("lat");
  return lat;
}

export const extractLon = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const lon = searchParams.get("lon");
  return lon;
}

export const extractLat1 = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const lat1 = searchParams.get("lat1");
  return lat1;
}

export const extractLat2 = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const lat2 = searchParams.get("lat2");
  return lat2;
}

export const extractLon1 = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const lon1 = searchParams.get("lon1");
  return lon1;
}

export const extractLon2 = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const lon2 = searchParams.get("lon2");
  return lon2;
}

export const extractSatelliteNumber = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  const satelliteNumber = searchParams.get("satnum");
  return satelliteNumber;
}