package com.gvozdev.codegen.spring2021.filestests.inputfiles;

import com.gvozdev.codegen.spring2021.entity.InputFile;
import com.gvozdev.codegen.spring2021.service.InputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class InputFilesLengthTests {

    @Autowired
    InputFileService service;

    @Test
    void shouldCheckArti6LengthInBytes() {
        InputFile inputFile = service.findByName("arti_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2191108;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckBshm10LengthInBytes() {
        InputFile inputFile = service.findByName("bshm_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2737693;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckGele6LengthInBytes() {
        InputFile inputFile = service.findByName("gele_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2094347;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckHueg10LengthInBytes() {
        InputFile inputFile = service.findByName("hueg_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2756811;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckIrkm6LengthInBytes() {
        InputFile inputFile = service.findByName("irkm_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2034960;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckKslv6LengthInBytes() {
        InputFile inputFile = service.findByName("kslv_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2060352;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckLeij10LengthInBytes() {
        InputFile inputFile = service.findByName("leij_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2829855;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckMaga6LengthInBytes() {
        InputFile inputFile = service.findByName("maga_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2187018;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckMenp6LengthInBytes() {
        InputFile inputFile = service.findByName("menp_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2206830;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckNovs6LengthInBytes() {
        InputFile inputFile = service.findByName("novs_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2188576;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckNoyb6LengthInBytes() {
        InputFile inputFile = service.findByName("noyb_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2277149;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckOnsa10LengthInBytes() {
        InputFile inputFile = service.findByName("onsa_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2969012;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckSpt10LengthInBytes() {
        InputFile inputFile = service.findByName("spt0_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 3265799;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckSvet6LengthInBytes() {
        InputFile inputFile = service.findByName("svet_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2220673;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckSvtl10LengthInBytes() {
        InputFile inputFile = service.findByName("svtl_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 3037494;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckTit10LengthInBytes() {
        InputFile inputFile = service.findByName("tit2_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2817943;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckVis10LengthInBytes() {
        InputFile inputFile = service.findByName("vis0_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2880381;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckVlad6LengthInBytes() {
        InputFile inputFile = service.findByName("vlad_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 1973532;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckWarn10LengthInBytes() {
        InputFile inputFile = service.findByName("warn_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2879015;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckYkts6LengthInBytes() {
        InputFile inputFile = service.findByName("ykts_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2197706;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckYusa6LengthInBytes() {
        InputFile inputFile = service.findByName("yusa_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 1972660;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckZeck10LengthInBytes() {
        InputFile inputFile = service.findByName("zeck_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2813022;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}
