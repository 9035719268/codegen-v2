package com.gvozdev.codegen.autumn2020.filestests.outputfiles;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Var3JavaFileWithOopFilesLengthTests {
    private final String var = "var3";
    private final String lang = "java";
    private final String file = "file";
    private final String oop = "withoop";

    @Autowired
    private OutputFileService service;

    @Test
    void mainLengthInBytes() {
        String fileName = "Main.java";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 25469;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void graphDrawerLengthInBytes() {
        String fileName = "GraphDrawer.java";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 4622;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void mainExampleLengthInBytes() {
        String fileName = "MainExample.java";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 25582;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void graphDrawerExampleLengthInBytes() {
        String fileName = "GraphDrawerExample.java";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 4713;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}