package com.gvozdev.codegen.spring2021.filestests.outputfiles;

import com.gvozdev.codegen.spring2021.entity.OutputFile;
import com.gvozdev.codegen.spring2021.service.OutputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class PythonFileNoOopFilesLengthTests {
    private final String lang = "python";
    private final String file = "file";
    private final String oop = "nooop";

    @Autowired
    private OutputFileService service;

    @Test
    void shouldCheckMainLengthInBytes() {
        String fileName = "main.py";
        OutputFile outputFile = service.findByParameters(lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 10284;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckMainExampleLengthInBytes() {
        String fileName = "mainExample.py";
        OutputFile outputFile = service.findByParameters(lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 10311;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}

