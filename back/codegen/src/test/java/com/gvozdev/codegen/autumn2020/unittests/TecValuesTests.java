package com.gvozdev.codegen.autumn2020.unittests;

import com.gvozdev.codegen.autumn2020.service.InputFileServiceImpl;
import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.domain.var3.IonoFileReader;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class TecValuesTests {
    private final String forecastFileName = "igrg0010.18i";
    private final String preciseFileName = "igsg0010.18i";
    private final int forecastFileFirstLine = 304;
    private final int preciseFileFirstLine = 394;

    @Autowired
    private InputFileServiceImpl service;

    @Test
    void shouldGetTecValuesFromLat0Lon0ForecastFile() {
        InputFile inputFile = service.findByName(forecastFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '0';
        char requiredSecondLatDigit = '.';
        char requiredThirdLatDigit = '0';
        String lon = "0";

        List<Integer> expected = asList(100, 71, 59, 61, 142, 238, 308, 311, 295, 258, 184, 177);
        List<Integer> actual = forecastFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, forecastFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLat0Lon0PreciseFile() {
        InputFile inputFile = service.findByName(preciseFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '0';
        char requiredSecondLatDigit = '.';
        char requiredThirdLatDigit = '0';
        String lon = "0";

        List<Integer> expected = asList(98, 73, 59, 59, 140, 238, 315, 323, 299, 257, 179, 174);
        List<Integer> actual = preciseFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, preciseFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLat0LonPositiveForecastFile() {
        InputFile inputFile = service.findByName(forecastFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '0';
        char requiredSecondLatDigit = '.';
        char requiredThirdLatDigit = '0';
        String lon = "20";

        List<Integer> expected = asList(73, 60, 51, 130, 198, 280, 366, 332, 277, 193, 163, 122);
        List<Integer> actual = forecastFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, forecastFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLat0LonPositivePreciseFile() {
        InputFile inputFile = service.findByName(preciseFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '0';
        char requiredSecondLatDigit = '.';
        char requiredThirdLatDigit = '0';
        String lon = "20";

        List<Integer> expected = asList(64, 50, 45, 123, 193, 282, 363, 329, 268, 186, 156, 117);
        List<Integer> actual = preciseFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, preciseFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLat0LonNegativeForecastFile() {
        InputFile inputFile = service.findByName(forecastFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '0';
        char requiredSecondLatDigit = '.';
        char requiredThirdLatDigit = '0';
        String lon = "-110";

        List<Integer> expected = asList(193, 117, 96, 78, 68, 55, 51, 114, 188, 267, 336, 306);
        List<Integer> actual = forecastFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, forecastFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLat0LonNegativePreciseFile() {
        InputFile inputFile = service.findByName(preciseFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '0';
        char requiredSecondLatDigit = '.';
        char requiredThirdLatDigit = '0';
        String lon = "-110";

        List<Integer> expected = asList(215, 113, 90, 78, 74, 62, 49, 114, 201, 269, 322, 306);
        List<Integer> actual = preciseFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, preciseFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatPositiveLon0ForecastFile() {
        InputFile inputFile = service.findByName(forecastFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '5';
        char requiredSecondLatDigit = '0';
        char requiredThirdLatDigit = '.';
        String lon = "0";

        List<Integer> expected = asList(45, 39, 31, 27, 49, 88, 74, 78, 51, 38, 33, 39);
        List<Integer> actual = forecastFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, forecastFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatPositiveLon0PreciseFile() {
        InputFile inputFile = service.findByName(preciseFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '5';
        char requiredSecondLatDigit = '0';
        char requiredThirdLatDigit = '.';
        String lon = "0";

        List<Integer> expected = asList(42, 39, 30, 28, 50, 87, 71, 80, 49, 34, 33, 37);
        List<Integer> actual = preciseFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, preciseFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatPositiveLonPositiveForecastFile() {
        InputFile inputFile = service.findByName(forecastFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '6';
        char requiredSecondLatDigit = '0';
        char requiredThirdLatDigit = '.';
        String lon = "35";

        List<Integer> expected = asList(22, 27, 18, 23, 53, 74, 58, 42, 23, 17, 24, 17);
        List<Integer> actual = forecastFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, forecastFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatPositiveLonPositivePreciseFile() {
        InputFile inputFile = service.findByName(preciseFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '6';
        char requiredSecondLatDigit = '0';
        char requiredThirdLatDigit = '.';
        String lon = "35";

        List<Integer> expected = asList(24, 28, 17, 21, 51, 74, 59, 41, 19, 14, 14, 14);
        List<Integer> actual = preciseFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, preciseFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatPositiveLonNegativeForecastFile() {
        InputFile inputFile = service.findByName(forecastFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '8';
        char requiredSecondLatDigit = '0';
        char requiredThirdLatDigit = '.';
        String lon = "-75";

        List<Integer> expected = asList(29, 21, 20, 18, 16, 17, 23, 26, 20, 21, 19, 17);
        List<Integer> actual = forecastFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, forecastFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatPositiveLonNegativePreciseFile() {
        InputFile inputFile = service.findByName(preciseFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '8';
        char requiredSecondLatDigit = '0';
        char requiredThirdLatDigit = '.';
        String lon = "-75";

        List<Integer> expected = asList(28, 21, 17, 11, 12, 13, 25, 23, 19, 18, 21, 16);
        List<Integer> actual = preciseFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, preciseFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatNegativeLon0ForecastFile() {
        InputFile inputFile = service.findByName(forecastFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '-';
        char requiredSecondLatDigit = '2';
        char requiredThirdLatDigit = '0';
        String lon = "0";

        List<Integer> expected = asList(84, 67, 54, 80, 144, 209, 262, 287, 225, 179, 122, 94);
        List<Integer> actual = forecastFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, forecastFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatNegativeLon0PreciseFile() {
        InputFile inputFile = service.findByName(preciseFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '-';
        char requiredSecondLatDigit = '2';
        char requiredThirdLatDigit = '0';
        String lon = "0";

        List<Integer> expected = asList(68, 57, 44, 73, 144, 204, 256, 296, 225, 175, 113, 84);
        List<Integer> actual = preciseFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, preciseFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatNegativeLonPositiveForecastFile() {
        InputFile inputFile = service.findByName(forecastFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '-';
        char requiredSecondLatDigit = '5';
        char requiredThirdLatDigit = '.';
        String lon = "45";

        List<Integer> expected = asList(56, 51, 104, 170, 232, 280, 325, 239, 166, 136, 140, 116);
        List<Integer> actual = forecastFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, forecastFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatNegativeLonPositivePreciseFile() {
        InputFile inputFile = service.findByName(preciseFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '-';
        char requiredSecondLatDigit = '5';
        char requiredThirdLatDigit = '.';
        String lon = "45";

        List<Integer> expected = asList(58, 52, 100, 175, 227, 281, 331, 258, 164, 134, 125, 110);
        List<Integer> actual = preciseFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, preciseFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatNegativeLonNegativeForecastFile() {
        InputFile inputFile = service.findByName(forecastFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '-';
        char requiredSecondLatDigit = '8';
        char requiredThirdLatDigit = '5';
        String lon = "-160";

        List<Integer> expected = asList(146, 122, 102, 93, 92, 76, 56, 57, 68, 84, 94, 109);
        List<Integer> actual = forecastFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, forecastFileFirstLine
        );
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTecValuesFromLatNegativeLonNegativePreciseFile() {
        InputFile inputFile = service.findByName(preciseFileName);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes);

        char requiredFirstLatDigit = '-';
        char requiredSecondLatDigit = '8';
        char requiredThirdLatDigit = '5';
        String lon = "-160";

        List<Integer> expected = asList(137, 114, 102, 88, 87, 69, 51, 55, 65, 76, 86, 102);
        List<Integer> actual = preciseFileReader.getTec(
            requiredFirstLatDigit, requiredSecondLatDigit, requiredThirdLatDigit,
            lon, preciseFileFirstLine
        );
        assertEquals(expected, actual);
    }
}
