package com.gvozdev.codegen.autumn2020.unittests;

import com.gvozdev.codegen.autumn2020.service.InputFileServiceImpl;
import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.domain.var3.EphemerisFileReader;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class IonCoefficientsTests {

    @Autowired
    private InputFileServiceImpl service;

    @Test
    void shouldGetAlphaCoefficients() {
        InputFile inputFile = service.findByName("brdc0010.18n");
        byte[] fileBytes = inputFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(fileBytes);
        List<Double> expected = asList(7.451E-9, -1.49E-8, -5.96E-8, 1.192E-7);
        List<Double> actual = ephemerisFileReader.getAlphaCoefficients();
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetBetaCoefficients() {
        InputFile inputFile = service.findByName("brdc0010.18n");
        byte[] fileBytes = inputFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(fileBytes);
        List<Double> expected = asList(92160.0, -114700.0, -131100.0, 720900.0);
        List<Double> actual = ephemerisFileReader.getBetaCoefficients();
        assertEquals(expected, actual);
    }
}
