package com.gvozdev.codegen.autumn2020.filestests.outputfiles;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Var1PythonFileWithOopFilesLengthTests {
    private final String var = "var1";
    private final String lang = "python";
    private final String file = "file";
    private final String oop = "withoop";

    @Autowired
    private OutputFileService service;

    @Test
    void mainLengthInBytes() {
        String fileName = "main.py";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 10108;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void mainExampleLengthInBytes() {
        String fileName = "mainExample.py";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 10132;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}