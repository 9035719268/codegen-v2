package com.gvozdev.codegen.autumn2020.unittests;

import com.gvozdev.codegen.autumn2020.service.InputFileServiceImpl;
import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.domain.var1.FileReader;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class SatelliteNumbersTests {

    @Autowired
    private InputFileServiceImpl service;

    @Test
    void shouldGetSatelliteNumbers() {
        InputFile inputFile = service.findByName("bshm_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        FileReader fileReader = new FileReader(fileBytes);
        List<Integer> expected = asList(
            2, 5, 6, 12, 15, 16, 18, 20, 21, 24, 25, 26, 29, 31
        );
        List<Integer> actual = fileReader.getSatelliteNumbers();
        assertEquals(expected, actual);
    }
}