package com.gvozdev.codegen.spring2021.filestests.outputfiles;

import com.gvozdev.codegen.spring2021.entity.OutputFile;
import com.gvozdev.codegen.spring2021.service.OutputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CppFileNoOopFilesLengthTests {
    private final String lang = "cpp";
    private final String file = "file";
    private final String oop = "nooop";

    @Autowired
    private OutputFileService service;

    @Test
    void shouldCheckMainLengthInBytes() {
        String fileName = "main.cpp";
        OutputFile outputFile = service.findByParameters(lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 9137;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckMainExampleLengthInBytes() {
        String fileName = "mainExample.cpp";
        OutputFile outputFile = service.findByParameters(lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 9189;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}
