package com.gvozdev.codegen.autumn2020.filestests.outputfiles;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Var2JavaManualWithOopFilesLengthTests {
    private final String var = "var2";
    private final String lang = "java";
    private final String file = "manual";
    private final String oop = "withoop";

    @Autowired
    private OutputFileService service;

    @Test
    void mainLengthInBytes() {
        String fileName = "Main.java";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 16112;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void graphDrawerLengthInBytes() {
        String fileName = "GraphDrawer.java";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 10931;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void mainExampleLengthInBytes() {
        String fileName = "MainExample.java";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 28194;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void graphDrawerExampleLengthInBytes() {
        String fileName = "GraphDrawerExample.java";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 23016;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}