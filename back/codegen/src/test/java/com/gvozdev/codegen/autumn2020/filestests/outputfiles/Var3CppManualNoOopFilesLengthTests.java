package com.gvozdev.codegen.autumn2020.filestests.outputfiles;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Var3CppManualNoOopFilesLengthTests {
    private final String var = "var3";
    private final String lang = "cpp";
    private final String file = "manual";
    private final String oop = "nooop";

    @Autowired
    private OutputFileService service;

    @Test
    void mainLengthInBytes() {
        String fileName = "main.cpp";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 8721;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void mainExampleLengthInBytes() {
        String fileName = "mainExample.cpp";
        OutputFile outputFile = service.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        int expected = 9338;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}