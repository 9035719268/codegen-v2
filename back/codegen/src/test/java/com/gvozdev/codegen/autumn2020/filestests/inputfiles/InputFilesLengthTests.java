package com.gvozdev.codegen.autumn2020.filestests.inputfiles;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class InputFilesLengthTests {

    @Autowired
    private InputFileServiceImpl service;

    @Test
    void shouldCheckBogi6LengthInBytes() {
        InputFile inputFile = service.findByName("BOGI_1-6.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 975441;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckBrdc0010LengthInBytes() {
        InputFile inputFile = service.findByName("brdc0010.18n");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 398728;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckBshm10LengthInBytes() {
        InputFile inputFile = service.findByName("bshm_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2727464;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckHueg6LengthInBytes() {
        InputFile inputFile = service.findByName("HUEG_1-6.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 1640852;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckHueg10LengthInBytes() {
        InputFile inputFile = service.findByName("hueg_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2746623;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckIgrg0010LengthInBytes() {
        InputFile inputFile = service.findByName("igrg0010.18i");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 862947;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckIgsg0010LengthInBytes() {
        InputFile inputFile = service.findByName("igsg0010.18i");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 870643;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckLeij10LengthInBytes() {
        InputFile inputFile = service.findByName("leij_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2819423;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckOnsa6LengthInBytes() {
        InputFile inputFile = service.findByName("ONSA_1-6.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 1682271;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckOnsa10LengthInBytes() {
        InputFile inputFile = service.findByName("onsa_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2958038;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckPots6LengthInBytes() {
        InputFile inputFile = service.findByName("POTS_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 1690371;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckSpt10LengthInBytes() {
        InputFile inputFile = service.findByName("spt0_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 3253827;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckSvtl10LengthInBytes() {
        InputFile inputFile = service.findByName("svtl_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 3026325;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckTit10LengthInBytes() {
        InputFile inputFile = service.findByName("tit2_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2807553;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckTitz6LengthInBytes() {
        InputFile inputFile = service.findByName("TITZ_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 1589836;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckVis10LengthInBytes() {
        InputFile inputFile = service.findByName("vis0_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2869674;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckWarn10LengthInBytes() {
        InputFile inputFile = service.findByName("warn_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2868401;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckWarn6LengthInBytes() {
        InputFile inputFile = service.findByName("WARN_6hours.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 1654061;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }

    @Test
    void shouldCheckZeck10LengthInBytes() {
        InputFile inputFile = service.findByName("zeck_1-10.dat");
        byte[] fileBytes = inputFile.getFileBytes();
        int expected = 2802665;
        int actual = fileBytes.length;
        assertEquals(expected, actual);
    }
}
