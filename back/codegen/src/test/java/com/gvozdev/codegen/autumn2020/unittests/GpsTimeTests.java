package com.gvozdev.codegen.autumn2020.unittests;

import com.gvozdev.codegen.autumn2020.service.InputFileServiceImpl;
import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.domain.var3.EphemerisFileReader;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class GpsTimeTests {

    @Autowired
    private InputFileServiceImpl service;

    @Test
    void singleDigitNumberSatellite() {
        InputFile inputFile = service.findByName("brdc0010.18n");
        byte[] fileBytes = inputFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(fileBytes);
        List<Double> expected = asList(
            80496.0, 90270.0, 93630.0, 100830.0, 108030.0, 0.0,
            124230.0, 129630.0, 136800.0, 144000.0, 151200.0, 158400.0
        );
        char requiredSatelliteNumber = '7';
        int requiredSatelliteNumberSize = 1;
        List<Double> actual = ephemerisFileReader.getGpsTime(
            requiredSatelliteNumber, requiredSatelliteNumberSize
        );
        assertEquals(expected, actual);
    }

    @Test
    void doubleDigitSatelliteNumber() {
        InputFile inputFile = service.findByName("brdc0010.18n");
        byte[] fileBytes = inputFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(fileBytes);
        List<Double> expected = asList(
            86400.0, 86430.0, 93600.0, 100800.0, 108000.0, 0.0,
            122400.0, 129600.0, 143460.0, 146160.0, 151230.0, 158430.0
        );
        char requiredSatelliteNumber1 = '1';
        char requiredSatelliteNumber2 = '4';
        int requiredSatelliteNumberSize = 2;
        List<Double> actual = ephemerisFileReader.getGpsTime(
            requiredSatelliteNumber1, requiredSatelliteNumber2, requiredSatelliteNumberSize
        );
        assertEquals(expected, actual);
    }
}
