package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EphemerisFileReader {
    private final byte[] fileBytes;
    private final List<List<List<Byte>>> allLines;
    private final int amountOfObservations;

    public EphemerisFileReader(byte[] fileBytes) {
        this.fileBytes = fileBytes;
        allLines = extractLinesOfData();
        amountOfObservations = 12;
    }

    public List<Double> getGpsTime(
        char requiredFirstSatelliteDigit, char requiredSecondSatelliteDigit,
        int requiredSatelliteNumberSize
    ) {
        List<Double> gpsTime = new ArrayList<>(Collections.nCopies(amountOfObservations, 0.0));
        int startOfObservations = 8;
        int linesPerObservation = 8;
        int allObservationLines = allLines.size();
        for (int observation = startOfObservations; observation < allObservationLines; observation += linesPerObservation) {
            try {
                List<Byte> satelliteNumberInfo = allLines.get(observation)
                                                         .get(0);
                char firstSatelliteDigit = (char)(byte)satelliteNumberInfo.get(0);
                char secondSatelliteDigit = (char)(byte)satelliteNumberInfo.get(1);
                int satelliteNumberSize = satelliteNumberInfo.size();
                List<Byte> hoursNumberInfo = allLines.get(observation)
                                                     .get(4);
                int hourFirstDigit = Character.getNumericValue(hoursNumberInfo.get(0));
                int numbersInHour = hoursNumberInfo.size();
                if (areEqual(firstSatelliteDigit, requiredFirstSatelliteDigit) &&
                    areEqual(secondSatelliteDigit, requiredSecondSatelliteDigit) &&
                    areEqual(satelliteNumberSize, requiredSatelliteNumberSize)
                ) {
                    int hourValue = getHourValue(numbersInHour, hourFirstDigit, hoursNumberInfo);
                    if (hourValue % 2 == 0) {
                        double numericGpsTime = getNumericGpsTime(observation);
                        int hour = hourValue / 2;
                        gpsTime.set(hour, numericGpsTime);
                    }
                }
            } catch (Exception ignored) { }
        }
        return gpsTime;
    }

    public List<Double> getGpsTime(
        char requiredSatelliteNumber, int requiredSatelliteNumberSize
    ) {
        List<Double> gpsTime = new ArrayList<>(Collections.nCopies(amountOfObservations, 0.0));
        int startOfObservations = 8;
        int linesPerObservation = 8;
        int allObservationLines = allLines.size();
        for (int observation = startOfObservations; observation < allObservationLines; observation += linesPerObservation) {
            try {
                List<Byte> satelliteNumberInfo = allLines.get(observation)
                                                         .get(0);
                char satelliteNumber = (char)(byte)satelliteNumberInfo.get(0);
                int satelliteNumberSize = satelliteNumberInfo.size();
                List<Byte> hoursNumberInfo = allLines.get(observation)
                                                     .get(4);
                int hourFirstDigit = Character.getNumericValue(hoursNumberInfo.get(0));
                int numbersInHour = hoursNumberInfo.size();
                if (areEqual(satelliteNumber, requiredSatelliteNumber) &&
                    areEqual(satelliteNumberSize, requiredSatelliteNumberSize)
                ) {
                    int hourValue = getHourValue(numbersInHour, hourFirstDigit, hoursNumberInfo);
                    if (hourValue % 2 == 0) {
                        double numericGpsTime = getNumericGpsTime(observation);
                        int hour = hourValue / 2;
                        gpsTime.set(hour, numericGpsTime);
                    }
                }
            } catch (Exception ignored) { }
        }
        return gpsTime;
    }

    // ===================================================================================================================
    // = Implementation
    // ===================================================================================================================

    private boolean isOneDigit(int number) {
        return number == 1;
    }

    private boolean areEqual(int number, int required) {
        return number == required;
    }

    private int getHourValue(
        int numbersInHour, int hourFirstNumber, List<Byte> hoursNumberInfo
    ) {
        int hourValue = 0;
        if (isOneDigit(numbersInHour)) {
            hourValue = hourFirstNumber;
        } else {
            hourValue = getDoubleDigitHourValue(hoursNumberInfo, hourFirstNumber);
        }
        return hourValue;
    }

    private int getDoubleDigitHourValue(List<Byte> hoursNumberInfo, int hourFirstNumber) {
        int hourSecondNumberIndex = 1;
        int hourSecondNumber = Character.getNumericValue(
            hoursNumberInfo.get(hourSecondNumberIndex)
        );
        String hourFull = hourFirstNumber + Integer.toString(hourSecondNumber);
        return Integer.parseInt(hourFull);
    }

    private double getNumericGpsTime(int observation) {
        StringBuilder numberBuilder = new StringBuilder();
        int observationOffset = 7;
        int digits = allLines.get(observation + observationOffset)
                             .get(0)
                             .size();
        for (int digit = 0; digit < digits; digit++) {
            char symbol = (char)allLines.get(observation + observationOffset)
                                        .get(0)
                                        .get(digit)
                                        .byteValue();
            if (symbol == 'D') {
                numberBuilder.append('E');
            } else {
                numberBuilder.append(symbol);
            }
        }
        return Double.parseDouble(numberBuilder.toString());
    }

    public List<Double> getAlphaCoefficients() {
        return extractIonCoefficients(3);
    }

    public List<Double> getBetaCoefficients() {
        return extractIonCoefficients(4);
    }

    private List<Double> extractIonCoefficients(int lineNumber) {
        List<List<Byte>> line = allLines.get(lineNumber);
        List<Double> coefficients = new ArrayList<>();
        int amountOfCoefficients = 4;
        for (int coefficient = 0; coefficient < amountOfCoefficients; coefficient++) {
            double numeric = getNumericCoefficient(line, coefficient);
            coefficients.add(numeric);
        }
        return coefficients;
    }

    private double getNumericCoefficient(List<List<Byte>> line, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int digits = line.get(number)
                         .size();
        for (int digit = 0; digit < digits; digit++) {
            char symbol = (char)line.get(number)
                                    .get(digit)
                                    .byteValue();
            if (symbol == 'D') {
                numberBuilder.append('E');
            } else {
                numberBuilder.append(symbol);
            }
        }
        return Double.parseDouble(numberBuilder.toString());
    }

    private List<List<List<Byte>>> extractLinesOfData() {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte newLine = 10;
        byte space = 32;

        for (byte symbol : fileBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == space)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if (symbol != space) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }
}
