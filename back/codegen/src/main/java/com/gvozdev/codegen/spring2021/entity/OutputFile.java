package com.gvozdev.codegen.spring2021.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "Spring2021OutputFile")
@Table(name = "spring_2021_output_file")
public class OutputFile implements Serializable {

    private static final long serialVersionUID = -7563540351916021549L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "lang", nullable = false)
    private String lang;

    @Column(name = "file", nullable = false)
    private String file;

    @Column(name = "oop", nullable = false)
    private String oop;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "file_bytes", nullable = false)
    private byte[] fileBytes;

    public OutputFile() {
    }

    public OutputFile(
        Long id, String lang, String file, String oop,
        String name, byte[] fileBytes
    ) {
        this.id = id;
        this.lang = lang;
        this.file = file;
        this.oop = oop;
        this.name = name;
        this.fileBytes = fileBytes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getOop() {
        return oop;
    }

    public void setOop(String oop) {
        this.oop = oop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }
}
