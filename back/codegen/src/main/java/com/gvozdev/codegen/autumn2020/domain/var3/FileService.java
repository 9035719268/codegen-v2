package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class FileService {
    private final EphemerisFileReader ephemerisFileReader;
    private final IonoFileReader forecastIonoFileReader;
    private final IonoFileReader preciseIonoFileReader;
    private final String satelliteNumber;

    public FileService(
        EphemerisFileReader ephemerisFileReader, IonoFileReader forecastIonoFileReader,
        IonoFileReader preciseIonoFileReader,String satelliteNumber
    ) {
        this.ephemerisFileReader = ephemerisFileReader;
        this.forecastIonoFileReader = forecastIonoFileReader;
        this.preciseIonoFileReader = preciseIonoFileReader;
        this.satelliteNumber = satelliteNumber;
    }

    public Components getComponents(String lat1, String lat2, String lon1, String lon2) {
        List<Double> gpsTimeValues = extractGpsTime();
        GpsTime gpsTime = createGpsTime(gpsTimeValues);

        List<List<Double>> ionCoefficients = extractIonCoefficients();

        List<Double> alphaCoefficients = ionCoefficients.get(0);
        Coefficients alpha = createCoefficients(alphaCoefficients);

        List<Double> betaCoefficients = ionCoefficients.get(1);
        Coefficients beta = createCoefficients(betaCoefficients);

        List<List<Integer>> a1TecValues = extractTecValues(lat2, lon2);
        List<List<Integer>> a2TecValues = extractTecValues(lat1, lon2);
        List<List<Integer>> a3TecValues = extractTecValues(lat1, lon1);
        List<List<Integer>> a4TecValues = extractTecValues(lat2, lon1);

        List<Integer> forecastA1TecValues = a1TecValues.get(0);
        TecValues forecastA1 = createTecValues(forecastA1TecValues);

        List<Integer> forecastA2TecValues = a2TecValues.get(0);
        TecValues forecastA2 = createTecValues(forecastA2TecValues);

        List<Integer> forecastA3TecValues = a3TecValues.get(0);
        TecValues forecastA3 = createTecValues(forecastA3TecValues);

        List<Integer> forecastA4TecValues = a4TecValues.get(0);
        TecValues forecastA4 = createTecValues(forecastA4TecValues);

        List<Integer> preciseA1TecValues = a1TecValues.get(1);
        TecValues preciseA1 = createTecValues(preciseA1TecValues);

        List<Integer> preciseA2TecValues = a2TecValues.get(1);
        TecValues preciseA2 = createTecValues(preciseA2TecValues);

        List<Integer> preciseA3TecValues = a3TecValues.get(1);
        TecValues preciseA3 = createTecValues(preciseA3TecValues);

        List<Integer> preciseA4TecValues = a4TecValues.get(1);
        TecValues preciseA4 = createTecValues(preciseA4TecValues);

        return new Components(
            gpsTime, alpha, beta,
            asList(forecastA1, forecastA2, forecastA3, forecastA4),
            asList(preciseA1, preciseA2, preciseA3, preciseA4)
        );
    }

    // =========================================================================================================================================================
    // Implementation
    // =========================================================================================================================================================

    private GpsTime createGpsTime(List<Double> gpsTimeValues) {
        return new GpsTime(gpsTimeValues);
    }

    private Coefficients createCoefficients(List<Double> coefficients) {
        return new Coefficients(coefficients);
    }

    private TecValues createTecValues(List<Integer> tecValues) {
        return new TecValues(tecValues);
    }

    private List<Double> extractGpsTime() {
        List<Double> gpsTimeArray;
        char firstDigit = satelliteNumber.charAt(0);
        if (satelliteNumber.length() > 1) {
            char secondDigit = satelliteNumber.charAt(1);
            int requiredSatelliteNumberSize = 2;
            gpsTimeArray = ephemerisFileReader.getGpsTime(
                firstDigit, secondDigit, requiredSatelliteNumberSize
            );
        } else {
            int requiredSatelliteNumberSize = 1;
            gpsTimeArray = ephemerisFileReader.getGpsTime(
                firstDigit, requiredSatelliteNumberSize
            );
        }
        return gpsTimeArray;
    }

    private List<List<Double>> extractIonCoefficients() {
        List<Double> alphaCoefficients = ephemerisFileReader.getAlphaCoefficients();
        List<Double> betaCoefficients = ephemerisFileReader.getBetaCoefficients();
        return asList(alphaCoefficients, betaCoefficients);
    }

    private List<List<Integer>> extractTecValues(String lat, String lon) {
        char firstDigit = lat.charAt(0);
        char secondDigit = lat.charAt(1);
        char thirdDigit = lat.charAt(2);
        int forecastTecListFirstLine = 304;
        int preciseTecListFirstLine = 394;
        List<List<Integer>> tecLists = new ArrayList<>();
        List<Integer> forecastTecList = forecastIonoFileReader.getTec(
            firstDigit, secondDigit, thirdDigit, lon, forecastTecListFirstLine
        );
        List<Integer> preciseTecList = preciseIonoFileReader.getTec(
            firstDigit, secondDigit, thirdDigit, lon, preciseTecListFirstLine
        );
        tecLists.add(forecastTecList);
        tecLists.add(preciseTecList);
        return tecLists;
    }
}
