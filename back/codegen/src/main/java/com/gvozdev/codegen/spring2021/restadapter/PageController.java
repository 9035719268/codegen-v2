package com.gvozdev.codegen.spring2021.restadapter;

import com.gvozdev.codegen.spring2021.domain.FileReader;
import com.gvozdev.codegen.spring2021.domain.FileService;
import com.gvozdev.codegen.spring2021.domain.Satellite;
import com.gvozdev.codegen.spring2021.entity.OutputFile;
import com.gvozdev.codegen.spring2021.service.InputFileService;
import com.gvozdev.codegen.spring2021.entity.InputFile;
import com.gvozdev.codegen.spring2021.service.OutputFileService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("Spring2021Controller")
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/year/2021/spring")
public class PageController {
    private InputFileService inputFileService;
    private OutputFileService outputFileService;

    public PageController(InputFileService inputFileService, OutputFileService outputFileService) {
        this.inputFileService = inputFileService;
        this.outputFileService = outputFileService;
    }

    @GetMapping(value = "/exercise-info", produces = "application/json")
    public List<Satellite> getSatellites(@RequestParam(value = "filename") String fileName) {
        InputFile inputFile = inputFileService.findByName(fileName);
        byte[] fileBytes = inputFile.getFileBytes();
        FileReader fileReader = new FileReader(fileBytes);
        FileService fileService = new FileService(fileReader);
        return fileService.getSatellites();
    }

    @GetMapping("/download/{lang}/{file}/{oop}/{filename}")
    public ResponseEntity<byte[]> downloadCode(
        @PathVariable(value = "lang") String lang,
        @PathVariable(value = "file") String file,
        @PathVariable(value = "oop") String oop,
        @PathVariable(value = "filename") String fileName
    ) {
        OutputFile outputFile = outputFileService.findByParameters(lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.valueOf("application/pdf"));
        responseHeaders.set("Content-Disposition", "attachment; filename=" + fileName);
        return new ResponseEntity<>(fileBytes, responseHeaders, HttpStatus.OK);
    }

    @GetMapping("/download/resources")
    public ResponseEntity<byte[]> downloadResources() {
        String fileName = "resources.rar";
        InputFile inputFile = inputFileService.findByName(fileName);
        byte[] fileBytes = inputFile.getFileBytes();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.valueOf("application/rar"));
        responseHeaders.set("Content-Disposition", "attachment; filename=" + fileName);
        return new ResponseEntity<>(fileBytes, responseHeaders, HttpStatus.OK);
    }
}
