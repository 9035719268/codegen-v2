package com.gvozdev.codegen.autumn2020.domain.var2;

import java.util.*;

import static java.util.Arrays.asList;

public class FileService {
    private final FileReader reader;

    public FileService(FileReader reader) {
        this.reader = reader;
    }

    public List<Satellite> getSatellites() {
        List<Integer> satelliteNumbers = extractSatelliteNumbers();
        int satellite1Number = satelliteNumbers.get(0);
        int satellite2Number = satelliteNumbers.get(1);
        int satellite3Number = satelliteNumbers.get(2);

        List<List<Double>> elevationAngles = extractElevationAngles(satelliteNumbers);
        List<Double> satellite1ElevationAngles = elevationAngles.get(0);
        List<Double> satellite2ElevationAngles = elevationAngles.get(1);
        List<Double> satellite3ElevationAngles = elevationAngles.get(2);

        Satellite satellite1 = createSatellite(satellite1Number, satellite1ElevationAngles);
        Satellite satellite2 = createSatellite(satellite2Number, satellite2ElevationAngles);
        Satellite satellite3 = createSatellite(satellite3Number, satellite3ElevationAngles);

        return asList(satellite1, satellite2, satellite3);
    }

    // =========================================================================================================================================================
    // Implementation
    // =========================================================================================================================================================

    private static Satellite createSatellite(
        int satelliteNumber, List<Double> satelliteElevationAngles
    ) {
        return new Satellite(satelliteNumber, satelliteElevationAngles);
    }

    private List<Integer> extractSatelliteNumbers() {
        int fromIndex = 0;
        int toIndex = 3;
        return reader.getSatelliteNumbers().subList(fromIndex, toIndex);
    }

    private List<List<Double>> extractElevationAngles(List<Integer> satelliteNumbers) {
        List<List<Double>> allElevationAngles = new ArrayList<>();
        for (int satelliteNumber : satelliteNumbers) {
            List<Double> elevationAngles;
            char lastDigit = toChar(satelliteNumber % 10);
            if (numberIsDoubleDigit(satelliteNumber)) {
                char firstDigit = toChar(satelliteNumber / 10);
                int satelliteNumberSize = 2;
                elevationAngles = reader.getElevationAngles(
                    firstDigit, lastDigit, satelliteNumberSize
                );
            } else {
                int satelliteNumberSize = 1;
                elevationAngles = reader.getElevationAngles(
                    lastDigit, satelliteNumberSize
                );
            }
            allElevationAngles.add(elevationAngles);
        }
        return allElevationAngles;
    }

    private boolean numberIsDoubleDigit(int number) {
        return number > 9;
    }

    private char toChar(int number) {
        return (char)(number + 48);
    }
}
