package com.gvozdev.codegen.autumn2020.service;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.repo.InputFileRepository;
import org.springframework.stereotype.Service;

@Service("Autumn2020InputFileService")
public class InputFileServiceImpl implements InputFileService {
    private InputFileRepository inputFileRepository;

    public InputFileServiceImpl(InputFileRepository inputFileRepository) {
        this.inputFileRepository = inputFileRepository;
    }

    @Override
    public InputFile findByName(String name) {
        return inputFileRepository.findByName(name);
    }
}
