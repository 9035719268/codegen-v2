package com.gvozdev.codegen.spring2021.service;

import com.gvozdev.codegen.spring2021.entity.OutputFile;
import com.gvozdev.codegen.spring2021.repo.OutputFileRepository;
import org.springframework.stereotype.Service;

@Service("Spring2021OutputFileService")
public class OutputFileServiceImpl implements OutputFileService {
    private OutputFileRepository outputFileRepository;

    public OutputFileServiceImpl(OutputFileRepository outputFileRepository) {
        this.outputFileRepository = outputFileRepository;
    }

    @Override
    public OutputFile findByParameters(String lang, String file, String oop, String name) {
        return outputFileRepository.findFileByParameters(lang, file, oop, name);
    }
}
