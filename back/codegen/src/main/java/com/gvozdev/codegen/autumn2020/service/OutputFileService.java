package com.gvozdev.codegen.autumn2020.service;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;

public interface OutputFileService {
    OutputFile findByParameters(String var, String lang, String file, String oop, String name);
}
