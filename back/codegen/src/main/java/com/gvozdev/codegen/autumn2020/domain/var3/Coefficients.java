package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.List;

public class Coefficients {
    private final List<Double> coefficients;

    public Coefficients(List<Double> coefficients) {
        this.coefficients = coefficients;
    }

    public List<Double> getCoefficients() {
        return coefficients;
    }
}
