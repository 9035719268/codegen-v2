package com.gvozdev.codegen.spring2021.service;

import com.gvozdev.codegen.spring2021.entity.InputFile;
import com.gvozdev.codegen.spring2021.repo.InputFileRepository;
import org.springframework.stereotype.Service;

@Service("Spring2021InputFileService")
public class InputFileServiceImpl implements InputFileService {
    private InputFileRepository inputFileRepository;

    public InputFileServiceImpl(InputFileRepository inputFileRepository) {
        this.inputFileRepository = inputFileRepository;
    }

    @Override
    public InputFile findByName(String name) {
        return inputFileRepository.findByName(name);
    }
}
