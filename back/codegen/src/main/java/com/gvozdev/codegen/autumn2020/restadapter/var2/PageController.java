package com.gvozdev.codegen.autumn2020.restadapter.var2;

import com.gvozdev.codegen.autumn2020.domain.var2.FileReader;
import com.gvozdev.codegen.autumn2020.domain.var2.FileService;
import com.gvozdev.codegen.autumn2020.domain.var2.Satellite;
import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("Autumn2020Var2Controller")
@RequestMapping("/year/2020/autumn/exercise-info/var2")
@CrossOrigin(origins = "http://localhost:3000")
public class PageController {
    private InputFileService service;

    public PageController(InputFileService service) {
        this.service = service;
    }

    @GetMapping(value = "", produces = "application/json")
    public List<Satellite> getSatellites(@RequestParam(value = "filename") String fileName) {
        InputFile inputFile = service.findByName(fileName);
        byte[] fileBytes = inputFile.getFileBytes();
        FileReader fileReader = new FileReader(fileBytes);
        FileService fileService = new FileService(fileReader);
        return fileService.getSatellites();
    }
}