package com.gvozdev.codegen.autumn2020.domain.var2;

import java.util.List;

public class Satellite {
    private final int number;
    private final List<Double> elevation;

    public Satellite(int number, List<Double> elevation) {
        this.number = number;
        this.elevation = elevation;
    }

    public int getNumber() {
        return number;
    }

    public List<Double> getElevation() {
        return elevation;
    }
}

