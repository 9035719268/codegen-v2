package com.gvozdev.codegen.autumn2020.service;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.repo.OutputFileRepository;
import org.springframework.stereotype.Service;

@Service("Autumn2020OutputFileService")
public class OutputFileServiceImpl implements OutputFileService {
    private OutputFileRepository outputFileRepository;

    public OutputFileServiceImpl(OutputFileRepository outputFileRepository) {
        this.outputFileRepository = outputFileRepository;
    }

    @Override
    public OutputFile findByParameters(String var, String lang, String file, String oop, String name) {
        return outputFileRepository.findFileByParameters(var, lang, file, oop, name);
    }
}
