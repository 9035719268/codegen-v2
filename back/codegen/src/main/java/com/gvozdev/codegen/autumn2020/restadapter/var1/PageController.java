package com.gvozdev.codegen.autumn2020.restadapter.var1;

import com.gvozdev.codegen.autumn2020.domain.var1.FileReader;
import com.gvozdev.codegen.autumn2020.domain.var1.FileService;
import com.gvozdev.codegen.autumn2020.domain.var1.Satellite;
import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("Autumn2020Var1Controller")
@RequestMapping("/year/2020/autumn/exercise-info/var1")
@CrossOrigin(origins = "http://localhost:3000")
public class PageController {
    private InputFileService service;

    public PageController(InputFileService service) {
        this.service = service;
    }

    @GetMapping(value = "", produces = "application/json")
    public List<Satellite> getSatellites(@RequestParam(value = "filename") String fileName) {
        InputFile inputFile = service.findByName(fileName);
        byte[] fileBytes = inputFile.getFileBytes();
        FileReader fileReader = new FileReader(fileBytes);
        FileService fileService = new FileService(fileReader);
        return fileService.getSatellites();
    }
}