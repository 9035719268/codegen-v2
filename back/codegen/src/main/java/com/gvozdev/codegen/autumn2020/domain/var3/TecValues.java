package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.List;

public class TecValues {
    private final List<Integer> tecValues;

    public TecValues(List<Integer> tecValues) {
        this.tecValues = tecValues;
    }

    public List<Integer> getTecValues() {
        return tecValues;
    }
}
