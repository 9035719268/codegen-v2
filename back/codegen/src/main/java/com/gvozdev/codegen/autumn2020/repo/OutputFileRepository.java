package com.gvozdev.codegen.autumn2020.repo;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository("Autumn2020OutputFileRepository")
public interface OutputFileRepository extends JpaRepository<OutputFile, Long> {

    @Query(
        value = "SELECT * FROM autumn_2020_output_file f " +
                "WHERE f.var = ?1 " +
                "AND f.lang = ?2 " +
                "AND f.file = ?3 " +
                "AND f.oop = ?4 " +
                "AND f.name = ?5 ",
        nativeQuery = true
    )
    OutputFile findFileByParameters(String var, String lang, String file, String oop, String name);
}
