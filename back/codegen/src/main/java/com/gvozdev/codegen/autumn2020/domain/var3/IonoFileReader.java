package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.ArrayList;
import java.util.List;

public class IonoFileReader {
    private final byte[] fileBytes;
    private final List<List<List<Byte>>> allLines;
    private final int amountOfObservations;

    public IonoFileReader(byte[] fileBytes) {
        this.fileBytes = fileBytes;
        allLines = extractLinesOfData();
        amountOfObservations = 12;
    }

    public List<Integer> getTec(
        char requiredFirstLatDigit, char requiredSecondLatDigit,
        char requiredThirdLatDigit, String lon, int firstLine
    ) {
        List<List<List<Integer>>> allTecForLat = new ArrayList<>();
        for (int line = firstLine; line < allLines.size(); line++) {
            try {
                char firstDigitOfLat = (char)(byte)allLines.get(line)
                                                           .get(0)
                                                           .get(0);
                char secondDigitOfLat = (char)(byte)allLines.get(line)
                                                            .get(0)
                                                            .get(1);
                char thirdDigitOfLat = (char)(byte)allLines.get(line)
                                                           .get(0)
                                                           .get(2);
                int linesWithTecPerLat = 5;
                if ((firstDigitOfLat == requiredFirstLatDigit) &&
                    (secondDigitOfLat == requiredSecondLatDigit) &&
                    (thirdDigitOfLat == requiredThirdLatDigit)) {
                    List<List<Integer>> tecPerLat = new ArrayList<>();
                    for (int lineWithTec = 1; lineWithTec <= linesWithTecPerLat; lineWithTec++) {
                        List<Integer> numbersLine = getLineOfTec(line, lineWithTec);
                        tecPerLat.add(numbersLine);
                    }
                    allTecForLat.add(tecPerLat);
                }
            } catch (Exception ignored) { }
        }
        int longtitude = Integer.parseInt(lon);
        return getTecArray(longtitude, allTecForLat);
    }

    private List<Integer> getTecArray(int lon, List<List<List<Integer>>> allTecForLat) {
        int lonFirst = -180;
        int dlon = 5;
        int tecValuesPerLine = 16;
        int row = Math.abs((lonFirst - lon) / (tecValuesPerLine * dlon));
        int pos = (Math.abs((lonFirst - lon) / dlon)) - (row * tecValuesPerLine);
        List<Integer> tecArray = new ArrayList<>();
        for (int observation = 0; observation < amountOfObservations; observation++) {
            int tec = allTecForLat.get(observation)
                                  .get(row)
                                  .get(pos);
            tecArray.add(tec);
        }
        return tecArray;
    }

    private List<Integer> getLineOfTec(int line, int lineWithTec) {
        List<Integer> numbersLine = new ArrayList<>();
        int numbersInRow = allLines.get(line + lineWithTec)
                                   .size();
        for (int number = 0; number < numbersInRow; number++) {
            int numeric = getNumericTec(line, lineWithTec, number);
            numbersLine.add(numeric);
        }
        return numbersLine;
    }

    private int getNumericTec(int line, int lineWithTec, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int numberLength = allLines.get(line + lineWithTec)
                                   .get(number)
                                   .size();
        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)allLines.get(line + lineWithTec)
                                        .get(number)
                                        .get(digit)
                                        .byteValue();
            numberBuilder.append(symbol);
        }
        return Integer.parseInt(numberBuilder.toString());
    }

    private List<List<List<Byte>>> extractLinesOfData() {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        int newLine = 10;
        int space = 32;

        for (byte symbol : fileBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == space)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if (symbol != space) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }
}
