package com.gvozdev.codegen.autumn2020.domain.var2;

import java.util.ArrayList;
import java.util.List;

public class FileReader {
    private final byte[] fileBytes;
    private final List<List<List<Byte>>> allLines;
    private final int amountOfObservations;

    public FileReader(byte[] fileBytes) {
        this.fileBytes = fileBytes;
        allLines = extractLinesOfData();
        amountOfObservations = 360;
    }

    public List<Integer> getSatelliteNumbers() {
        List<Integer> satelliteNumbers = new ArrayList<>();
        char smallestSingleFigure = '1';
        char biggestSingleFigure = '9';
        int satelliteNumberSize = 1;
        for (char satelliteNumber = smallestSingleFigure; satelliteNumber <= biggestSingleFigure; satelliteNumber++) {
            List<List<Double>> measurements = getMeasurements(satelliteNumber, satelliteNumberSize);
            if (measurements.size() >= amountOfObservations) {
                int requiredSatelliteNumber = Character.getNumericValue(satelliteNumber);
                satelliteNumbers.add(requiredSatelliteNumber);
            }
        }

        int smallestDoubleFigure = 10;
        int biggestSatelliteNumber = 40;
        satelliteNumberSize = 2;
        for (int satelliteNumber = smallestDoubleFigure; satelliteNumber <= biggestSatelliteNumber; satelliteNumber++) {
            String satellite = String.valueOf(satelliteNumber);
            char firstDigit = satellite.charAt(0);
            char secondDigit = satellite.charAt(1);
            List<List<Double>> measurements = getMeasurements(
                firstDigit, secondDigit,
                satelliteNumberSize
            );
            if (measurements.size() >= amountOfObservations) {
                int requiredSatelliteNumber = Integer.parseInt(
                    String.valueOf(firstDigit) + String.valueOf(secondDigit)
                );
                satelliteNumbers.add(requiredSatelliteNumber);
            }
        }
        return satelliteNumbers;
    }

    public List<Double> getElevationAngles(
        char requiredSatelliteNumber1, char requiredSatelliteNumber2,
        int requiredSatelliteNumberSize
    ) {
        List<List<Double>> measurements = getMeasurements(
            requiredSatelliteNumber1, requiredSatelliteNumber2,
            requiredSatelliteNumberSize
        );
        List<Double> elevationAngles = new ArrayList<>();
        int elevationAngleIndex = 14;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double elevationAngle = measurements.get(observation)
                                                .get(elevationAngleIndex);
            elevationAngles.add(elevationAngle);
        }
        return elevationAngles;
    }

    public List<Double> getElevationAngles(
        char requiredSatelliteNumber, int requiredSatelliteNumberSize
    ) {
        List<List<Double>> measurements = getMeasurements(requiredSatelliteNumber, requiredSatelliteNumberSize);
        List<Double> elevationAngles = new ArrayList<>();
        int elevationAngleIndex = 14;
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double elevationAngle = measurements.get(observation)
                                                .get(elevationAngleIndex);
            elevationAngles.add(elevationAngle);
        }
        return elevationAngles;
    }

    private List<List<Double>> getMeasurements(
        char requiredSatelliteNumber1, char requiredSatelliteNumber2,
        int requiredSatelliteNumberSize
    ) {
        List<List<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;
        for (List<List<Byte>> line : allLines) {
            try {
                char satelliteNumber1 = (char)(byte)line.get(0)
                                                        .get(0);
                char satelliteNumber2 = (char)(byte)line.get(0)
                                                        .get(1);
                int satelliteNumberSize = line.get(0)
                                              .size();
                if ((satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    List<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) { }
        }
        return measurements;
    }

    private List<List<Double>> getMeasurements(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        List<List<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;
        for (List<List<Byte>> line : allLines) {
            try {
                char satelliteNumber = (char)(byte)line.get(0)
                                                       .get(0);
                int satelliteNumberSize = line.get(0)
                                              .size();

                if ((satelliteNumber == requiredSatelliteNumber) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    List<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) { }
        }
        return measurements;
    }

    private double getNumeric(List<List<Byte>> line, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int numberLength = line.get(number).size();
        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)line.get(number).get(digit).byteValue();
            numberBuilder.append(symbol);
        }
        return Double.parseDouble(numberBuilder.toString());
    }

    private List<List<List<Byte>>> extractLinesOfData() {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte tab = 9;
        byte newLine = 10;
        byte carriageReturn = 13;
        byte space = 32;

        for (byte symbol : fileBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == tab)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }
}
