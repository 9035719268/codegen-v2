package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.List;

public class Components {
    private final GpsTime gpsTime;
    private final Coefficients alpha;
    private final Coefficients beta;
    private final List<TecValues> forecastValues;
    private final List<TecValues> preciseValues;

    public Components(
        GpsTime gpsTime, Coefficients alpha, Coefficients beta,
        List<TecValues> forecastValues, List<TecValues> preciseValues
    ) {
        this.gpsTime = gpsTime;
        this.alpha = alpha;
        this.beta = beta;
        this.forecastValues = forecastValues;
        this.preciseValues = preciseValues;
    }

    public GpsTime getGpsTime() {
        return gpsTime;
    }

    public Coefficients getAlpha() {
        return alpha;
    }

    public Coefficients getBeta() {
        return beta;
    }

    public List<TecValues> getForecastValues() {
        return forecastValues;
    }

    public List<TecValues> getPreciseValues() {
        return preciseValues;
    }
}
