package com.gvozdev.codegen.autumn2020.restadapter;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileService;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("Autumn2020FileDownloadingController")
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/year/2020/autumn/download")
public class FileDownloadingController {
    private InputFileService inputFileService;
    private OutputFileService outputFileService;

    public FileDownloadingController(InputFileService inputFileService, OutputFileService outputFileService) {
        this.inputFileService = inputFileService;
        this.outputFileService = outputFileService;
    }

    @GetMapping("/{var}/{lang}/{file}/{oop}/{filename}")
    public ResponseEntity<byte[]> downloadCode(
        @PathVariable(value = "var") String var,
        @PathVariable(value = "lang") String lang,
        @PathVariable(value = "file") String file,
        @PathVariable(value = "oop") String oop,
        @PathVariable(value = "filename") String fileName
    ) {
        OutputFile outputFile = outputFileService.findByParameters(var, lang, file, oop, fileName);
        byte[] fileBytes = outputFile.getFileBytes();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.valueOf("application/pdf"));
        responseHeaders.set("Content-Disposition", "attachment; filename=" + fileName);
        return new ResponseEntity<>(fileBytes, responseHeaders, HttpStatus.OK);
    }

    @GetMapping("/resources")
    public ResponseEntity<byte[]> downloadResources() {
        String fileName = "resources.rar";
        InputFile inputFile = inputFileService.findByName(fileName);
        byte[] fileBytes = inputFile.getFileBytes();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.valueOf("application/rar"));
        responseHeaders.set("Content-Disposition", "attachment; filename=" + fileName);
        return new ResponseEntity<>(fileBytes, responseHeaders, HttpStatus.OK);
    }
}
