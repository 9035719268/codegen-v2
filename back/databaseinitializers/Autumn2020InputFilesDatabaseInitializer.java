package com.gvozdev.codegen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

//@Component
public class Autumn2020InputFilesDatabaseInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Autumn2020InputFilesDatabaseInitializer.class);
    private static final String PATH = "src/main/resources/files/2020/autumn/input/";

    @Autowired
    private Autumn2020InputFileRepository repository;

//    @PostConstruct
    @Transactional
    public void init() {
        saveFile(1L, "BOGI_1-6.dat");
        saveFile(2L, "brdc0010.18n");
        saveFile(3L, "bshm_1-10.dat");
        saveFile(4L, "HUEG_1-6.dat");
        saveFile(5L, "hueg_1-10.dat");
        saveFile(6L, "igrg0010.18i");
        saveFile(7L, "igsg0010.18i");
        saveFile(8L, "leij_1-10.dat");
        saveFile(9L, "ONSA_1-6.dat");
        saveFile(10L, "onsa_1-10.dat");
        saveFile(11L, "POTS_6hours.dat");
        saveFile(12L, "spt0_1-10.dat");
        saveFile(13L, "svtl_1-10.dat");
        saveFile(14L, "tit2_1-10.dat");
        saveFile(15L, "TITZ_6hours.dat");
        saveFile(16L, "vis0_1-10.dat");
        saveFile(17L, "warn_1-10.dat");
        saveFile(18L, "WARN_6hours.dat");
        saveFile(19L, "zeck_1-10.dat");
        saveFile(20L, "resources.rar");
        LOGGER.info("Все входные файлы для УИРС осени 2020 загружены");
    }


    // ===================================================================================================================
    // = Implementation
    // ===================================================================================================================

    private void saveFile(Long id, String fileName) {
        try {
            File file = new File(PATH + fileName);
            byte[] fileBytes = Files.readAllBytes(file.toPath());
            Autumn2020InputFile inputFile = new Autumn2020InputFile(id, fileName, fileBytes);
            repository.save(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
