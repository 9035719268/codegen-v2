package manual;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.List;

import static java.util.Arrays.asList;

public class GraphDrawer extends Application {
    public static void draw() {
        launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        int amountOfObservations = 360;

        int satellite1Number = ?;
        int satellite2Number = ?;
        int satellite3Number = ?;

        List<Double> satellite1Md = asList( ? );

        List<Double> satellite1Td = asList( ? );

        List<Double> satellite1Mw = asList( ? );

        List<Double> satellite1Tw = asList( ? );

        List<Double> satellite2Md = asList( ? );

        List<Double> satellite2Td = asList( ? );

        List<Double> satellite2Mw = asList( ? );

        List<Double> satellite2Tw = asList( ? );

        List<Double> satellite3Md = asList ( ? );

        List<Double> satellite3Td = asList( ? );

        List<Double> satellite3Mw = asList( ? );

        List<Double> satellite3Tw = asList( ? );

        List<Double> satellite1Elevation = asList( ? );

        List<Double> satellite2Elevation = asList( ? );

        List<Double> satellite3Elevation = asList( ? );

        SatelliteFactory satelliteFactory = new SatelliteFactory(amountOfObservations);

        Satellite satellite1 = satelliteFactory.createSatellite(satellite1Number, satellite1Md, satellite1Td, satellite1Mw, satellite1Tw,
                                                                satellite1Elevation);
        Satellite satellite2 = satelliteFactory.createSatellite(satellite2Number, satellite2Md, satellite2Td, satellite2Mw, satellite2Tw,
                                                                satellite2Elevation);
        Satellite satellite3 = satelliteFactory.createSatellite(satellite3Number, satellite3Md, satellite3Td, satellite3Mw, satellite3Tw,
                                                                satellite3Elevation);

        DrawerTemplate troposphericDelayDrawer = new TroposphericDelayDrawer(satellite1, satellite2, satellite3, amountOfObservations);
        troposphericDelayDrawer.draw();

        DrawerTemplate elevationAnglesDrawer = new ElevationAnglesDrawer(satellite1, satellite2, satellite3, amountOfObservations);
        elevationAnglesDrawer.draw();
    }
}


abstract class DrawerTemplate {
    protected final Satellite satellite1;
    protected final Satellite satellite2;
    protected final Satellite satellite3;
    private final int amountOfObservations;

    protected DrawerTemplate(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
        this.amountOfObservations = amountOfObservations;
    }

    @SuppressWarnings("unchecked")
    protected void draw() {
        Stage stage = new Stage();
        HBox root = new HBox();
        Scene scene = new Scene(root, 450, 330);

        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Время");

        NumberAxis yAxis = new NumberAxis();
        String yAxisLabel = getYAxisLabel();
        yAxis.setLabel(yAxisLabel);

        LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        XYChart.Series<Number, Number> measurements1 = new XYChart.Series<>();
        int satellite1Number = satellite1.getNumber();
        measurements1.setName("Спутник #" + satellite1Number);
        XYChart.Series<Number, Number> measurements2 = new XYChart.Series<>();
        int satellite2Number = satellite2.getNumber();
        measurements2.setName("Спутник #" + satellite2Number);
        XYChart.Series<Number, Number> measurements3 = new XYChart.Series<>();
        int satellite3Number = satellite3.getNumber();
        measurements3.setName("Спутник #" + satellite3Number);

        List<Double> measurements1Array = getSatellite1Measurements();
        List<Double> measurements2Array = getSatellite2Measurements();
        List<Double> measurements3Array = getSatellite3Measurements();

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1Data = measurements1Array.get(observation);
            measurements1.getData().add(new XYChart.Data<>(observation, satellite1Data));

            double satellite2Data = measurements2Array.get(observation);
            measurements2.getData().add(new XYChart.Data<>(observation, satellite2Data));

            double satellite3Data = measurements3Array.get(observation);
            measurements3.getData().add(new XYChart.Data<>(observation, satellite3Data));
        }

        lineChart.getData().addAll(measurements1, measurements2, measurements3);
        root.getChildren().add(lineChart);

        stage.setScene(scene);
        stage.show();
    }

    abstract String getYAxisLabel();
    abstract List<Double> getSatellite1Measurements();
    abstract List<Double> getSatellite2Measurements();
    abstract List<Double> getSatellite3Measurements();
}

class TroposphericDelayDrawer extends DrawerTemplate {
    public TroposphericDelayDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(satellite1, satellite2, satellite3, amountOfObservations);
    }

    @Override
    String getYAxisLabel() {
        return "Тропосферная задержка сигнала, метры";
    }

    @Override
    List<Double> getSatellite1Measurements() {
        return satellite1.getTroposphericDelays();
    }

    @Override
    List<Double> getSatellite2Measurements() {
        return satellite2.getTroposphericDelays();
    }

    @Override
    List<Double> getSatellite3Measurements() {
        return satellite3.getTroposphericDelays();
    }
}

class ElevationAnglesDrawer extends DrawerTemplate {
    public ElevationAnglesDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(satellite1, satellite2, satellite3, amountOfObservations);
    }

    @Override
    String getYAxisLabel() {
        return "Угол возвышения спутника, полуциклы";
    }

    @Override
    List<Double> getSatellite1Measurements() {
        return satellite1.getElevationAngles();
    }

    @Override
    List<Double> getSatellite2Measurements() {
        return satellite2.getElevationAngles();
    }

    @Override
    List<Double> getSatellite3Measurements() {
        return satellite3.getElevationAngles();
    }
}