package manual;

import file.GraphDrawer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class Main {
    public static void main(String[] args) {
        int amountOfObservations = 360;

        int satellite1Number = ?;
        int satellite2Number = ?;
        int satellite3Number = ?;

        List<Double> satellite1Md = asList( ? );

        List<Double> satellite1Td = asList( ? );

        List<Double> satellite1Mw = asList( ? );

        List<Double> satellite1Tw = asList( ? );

        List<Double> satellite2Md = asList( ? );

        List<Double> satellite2Td = asList( ? );

        List<Double> satellite2Mw = asList( ? );

        List<Double> satellite2Tw = asList( ? );

        List<Double> satellite3Md = asList ( ? );

        List<Double> satellite3Td = asList( ? );

        List<Double> satellite3Mw = asList( ? );

        List<Double> satellite3Tw = asList( ? );

        List<Double> satellite1Elevation = asList( ? );

        List<Double> satellite2Elevation = asList( ? );

        List<Double> satellite3Elevation = asList( ? );

        SatelliteFactory satelliteFactory = new SatelliteFactory(amountOfObservations);

        Satellite satellite1 = satelliteFactory.createSatellite(satellite1Number, satellite1Md, satellite1Td, satellite1Mw, satellite1Tw,
                                                                satellite1Elevation);
        Satellite satellite2 = satelliteFactory.createSatellite(satellite2Number, satellite2Md, satellite2Td, satellite2Mw, satellite2Tw,
                                                                satellite2Elevation);
        Satellite satellite3 = satelliteFactory.createSatellite(satellite3Number, satellite3Md, satellite3Td, satellite3Mw, satellite3Tw,
                                                                satellite3Elevation);

        ConsoleOutput consoleOutput = new ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        consoleOutput.printInfo();

        GraphDrawer.draw();
    }
}

class ConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;
    private final int amountOfObservations;

    public ConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
        this.amountOfObservations = amountOfObservations;
    }

    public void printInfo() {
        int satellite1Number = satellite1.getNumber();
        int satellite2Number = satellite2.getNumber();
        int satellite3Number = satellite3.getNumber();
        List<Double> satellite1TroposphericDelays = satellite1.getTroposphericDelays();
        List<Double> satellite2TroposphericDelays = satellite2.getTroposphericDelays();
        List<Double> satellite3TroposphericDelays = satellite3.getTroposphericDelays();
        List<Double> satellite1ElevationAngles = satellite1.getElevationAngles();
        List<Double> satellite2ElevationAngles = satellite2.getElevationAngles();
        List<Double> satellite3ElevationAngles = satellite3.getElevationAngles();
        System.out.println("Спутник #" + satellite1Number +
                           "\t\t\t\t\t\t\tСпутник #" + satellite2Number +
                           "\t\t\t\t\t\t\tСпутник #" + satellite3Number);
        DecimalFormat df = new DecimalFormat("#.##########");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1TroposphericDelay = satellite1TroposphericDelays.get(observation);
            double satellite1ElevationAngle = satellite1ElevationAngles.get(observation);
            double satellite2TroposphericDelay = satellite2TroposphericDelays.get(observation);
            double satellite2ElevationAngle = satellite2ElevationAngles.get(observation);
            double satellite3TroposphericDelay = satellite3TroposphericDelays.get(observation);
            double satellite3ElevationAngle = satellite3ElevationAngles.get(observation);
            System.out.println(df.format(satellite1TroposphericDelay) + "\t" + df.format(satellite1ElevationAngle) + "\t\t" +
                               df.format(satellite2TroposphericDelay) + "\t" + df.format(satellite2ElevationAngle) + "\t\t" +
                               df.format(satellite3TroposphericDelay) + "\t" + df.format(satellite3ElevationAngle));
        }
    }
}

@SuppressWarnings("UnnecessaryLocalVariable")
class SatelliteFactory {
    private final int amountOfObservations;

    public SatelliteFactory(int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
    }

    public Satellite createSatellite(int satelliteNumber, List<Double> mdArray, List<Double> tdArray,
                                     List<Double> mwArray, List<Double> twArray, List<Double> elevationArray) {
        DelayComponent dryComponentFunction = new DryComponentFunction(mdArray);
        DelayComponent verticalDryComponent = new VerticalDryComponent(tdArray);
        DelayComponent wetComponentFunction = new WetComponentFunction(mwArray);
        DelayComponent verticalWetComponent = new VerticalWetComponent(twArray);
        TroposphericDelays delay = new TroposphericDelays(dryComponentFunction, verticalDryComponent,
                                                          wetComponentFunction, verticalWetComponent,
                                                          amountOfObservations);
        ElevationAngles angles = new ElevationAngles(elevationArray);
        Satellite satellite = new Satellite(satelliteNumber, delay, angles);
        return satellite;
    }
}

class Satellite {
    private final int number;
    private final TroposphericDelays troposphericDelays;
    private final ElevationAngles elevationAngles;

    public Satellite(int number, TroposphericDelays troposphericDelays, ElevationAngles elevationAngles) {
        this.number = number;
        this.troposphericDelays = troposphericDelays;
        this.elevationAngles = elevationAngles;
    }

    public int getNumber() {
        return number;
    }

    public List<Double> getTroposphericDelays() {
        return troposphericDelays.getDelays();
    }

    public List<Double> getElevationAngles() {
        return elevationAngles.getAnglesInHalfCircles();
    }
}

class TroposphericDelays {
    private final DelayComponent dryComponentFunction;
    private final DelayComponent verticalDryComponent;
    private final DelayComponent wetComponentFunction;
    private final DelayComponent verticalWetComponent;
    private final int amountOfObservations;
    private final List<Double> delays;

    public TroposphericDelays(DelayComponent dryComponentFunction, DelayComponent verticalDryComponent,
                              DelayComponent wetComponentFunction, DelayComponent verticalWetComponent,
                              int amountOfObservations) {
        this.dryComponentFunction = dryComponentFunction;
        this.verticalDryComponent = verticalDryComponent;
        this.wetComponentFunction = wetComponentFunction;
        this.verticalWetComponent = verticalWetComponent;
        this.amountOfObservations = amountOfObservations;
        delays = new ArrayList<>();
    }

    public List<Double> getDelays() {
        List<Double> dryComponentFunctionValues = dryComponentFunction.getValues();
        List<Double> verticalDryComponentValues = verticalDryComponent.getValues();
        List<Double> wetComponentFunctionValues = wetComponentFunction.getValues();
        List<Double> verticalWetComponentValues = verticalWetComponent.getValues();

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double md = dryComponentFunctionValues.get(observation);
            double td = verticalDryComponentValues.get(observation);
            double mw = wetComponentFunctionValues.get(observation);
            double tw = verticalWetComponentValues.get(observation);
            double delay = md * td + mw * tw;
            delays.add(delay);
        }
        return delays;
    }
}

interface DelayComponent {
    List<Double> getValues();
}

class DryComponentFunction implements DelayComponent {
    private final List<Double> values;

    public DryComponentFunction(List<Double> values) {
        this.values = values;
    }

    @Override
    public List<Double> getValues() {
        return values;
    }
}

class VerticalDryComponent implements DelayComponent {
    private final List<Double> values;

    public VerticalDryComponent(List<Double> values) {
        this.values = values;
    }

    @Override
    public List<Double> getValues() {
        return values;
    }
}

class WetComponentFunction implements DelayComponent {
    private final List<Double> values;

    public WetComponentFunction(List<Double> values) {
        this.values = values;
    }

    @Override
    public List<Double> getValues() {
        return values;
    }
}

class VerticalWetComponent implements DelayComponent {
    private final List<Double> values;

    public VerticalWetComponent(List<Double> values) {
        this.values = values;
    }

    @Override
    public List<Double> getValues() {
        return values;
    }
}

class ElevationAngles {
    private final List<Double> degrees;

    public ElevationAngles(List<Double> degrees) {
        this.degrees = degrees;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public List<Double> getAnglesInHalfCircles() {
        double halfCircle = 180;
        List<Double> halfCircles = degrees.stream()
                                               .map(angleInDegrees -> angleInDegrees / halfCircle)
                                               .collect(toList());
        return halfCircles;
    }
}