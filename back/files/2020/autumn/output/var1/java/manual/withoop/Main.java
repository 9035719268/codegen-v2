package

import java.text.DecimalFormat;
import java.util.List;

import static java.util.Arrays.asList;

?;

public class Main {
    public static void main(String[] args) {
        int amountOfObservations = 360;

        List<Double> satellite1P1 = asList( ? );

        List<Double> satellite1P2 = asList( ? );

        List<Double> satellite2P1 = asList( ? );

        List<Double> satellite2P2 = asList( ? );

        List<Double> satellite3P1 = asList( ? );

        List<Double> satellite3P2 = asList( ? );

        Satellite satellite1 = SatelliteFactory.createSatellite(?, satellite1P1, satellite1P2);
        Satellite satellite2 = SatelliteFactory.createSatellite(?, satellite2P1, satellite2P2);
        Satellite satellite3 = SatelliteFactory.createSatellite(?, satellite3P1, satellite3P2);

        ConsoleOutput consoleOutput = new ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        consoleOutput.printDelays();

        GraphDrawer.draw();
    }
}


class ConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;
    private final int amountOfObservations;

    public ConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
        this.amountOfObservations = amountOfObservations;
    }

    public void printDelays() {
        int satellite1Number = satellite1.getNumber();
        int satellite2Number = satellite2.getNumber();
        int satellite3Number = satellite3.getNumber();
        System.out.println("Спутник #" + satellite1Number + "\tСпутник #" + satellite2Number + "\tСпутник #" + satellite3Number);
        DecimalFormat df = new DecimalFormat("#.##########");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double delay1 = satellite1.getIonosphericDelayAt(observation);
            double delay2 = satellite2.getIonosphericDelayAt(observation);
            double delay3 = satellite3.getIonosphericDelayAt(observation);
            System.out.println(df.format(delay1) + "\t" + df.format(delay2) + "\t" + df.format(delay3));
        }
    }
}


class SatelliteFactory {
    public static Satellite createSatellite(int number, List<Double> p1Arr, List<Double> p2Arr) {
        PseudoRange p1 = new PseudoRange(p1Arr);
        PseudoRange p2 = new PseudoRange(p2Arr);
        IonosphericDelay delay = new IonosphericDelay(p1, p2);
        Satellite satellite = new Satellite(number, delay);
        return satellite;
    }
}


class Satellite {
    private final int number;
    private final IonosphericDelay ionosphericDelay;

    public Satellite(int number, IonosphericDelay ionosphericDelay) {
        this.number = number;
        this.ionosphericDelay = ionosphericDelay;
    }

    public int getNumber() {
        return number;
    }

    public double getIonosphericDelayAt(int observation) {
        return ionosphericDelay.getIonosphericDelayAt(observation);
    }
}


class IonosphericDelay {
    private final PseudoRange pseudoRange1;
    private final PseudoRange pseudoRange2;

    public IonosphericDelay(PseudoRange pseudoRange1, PseudoRange pseudoRange2) {
        this.pseudoRange1 = pseudoRange1;
        this.pseudoRange2 = pseudoRange2;
    }

    public double getIonosphericDelayAt(int observation) {
        double speedOfLight = 2.99792458 * 1E8;
        double p1 = pseudoRange1.getPseudoRangeAt(observation);
        double p2 = pseudoRange2.getPseudoRangeAt(observation);
        double k = getK();
        double delay = (p1 - p2) / (speedOfLight * (1 - k));
        double delayInMeters = delay * speedOfLight;
        return delayInMeters;
    }

    private double getK() {
        double f1 = 1_575_420_000;
        double f2 = 1_227_600_000;
        double k = Math.pow(f1, 2) / Math.pow(f2, 2);
        return k;
    }
}


class PseudoRange {
    private final List<Double> pseudoRanges;

    public PseudoRange(List<Double> pseudoRanges) {
        this.pseudoRanges = pseudoRanges;
    }

    public double getPseudoRangeAt(int observation) {
        return pseudoRanges.get(observation);
    }
}