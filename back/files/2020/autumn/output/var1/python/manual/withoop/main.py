import matplotlib.pyplot as plt
import numpy as np


class PseudoRange:
    def __init__(self, pseudoRanges: list) -> None:
        self.__pseudoRanges = pseudoRanges

    def __getitem__(self, observation: int) -> float:
        return self.__pseudoRanges[observation]


class IonosphericDelay:
    def __init__(self, pseudoRange1: PseudoRange, pseudoRange2: PseudoRange) -> None:
        self.__pseudoRange1 = pseudoRange1
        self.__pseudoRange2 = pseudoRange2

    def __getitem__(self, interval: int) -> float:
        speedOfLight: float = 2.99792458 * 1E8
        p1: float = self.__pseudoRange1[interval]
        p2: float = self.__pseudoRange2[interval]
        k: float = self.__getK()
        delay: float = (p1 - p2) / (speedOfLight * (1 - k))
        delayInMeters: float = delay * speedOfLight
        return delayInMeters

    @classmethod
    def __getK(cls) -> float:
        f1: float = 1_575_420_000
        f2: float = 1_227_600_000
        k: float = pow(f1, 2) / pow(f2, 2)
        return k


class Satellite:
    def __init__(self, number: int, ionosphericDelay: IonosphericDelay) -> None:
        self.__number = number
        self.__ionosphericDelay = ionosphericDelay

    @property
    def number(self) -> int:
        return self.__number

    @property
    def ionosphericDelay(self) -> IonosphericDelay:
        return self.__ionosphericDelay


class SatelliteFactory:
    @staticmethod
    def createSatellite(number: int, p1Array: list, p2Array: list) -> Satellite:
        p1: PseudoRange = PseudoRange(p1Array)
        p2: PseudoRange = PseudoRange(p2Array)
        delay: IonosphericDelay = IonosphericDelay(p1, p2)
        satellite: Satellite = Satellite(number, delay)
        return satellite


class ConsoleOutput:
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations: int = amountOfObservations

    def printDelays(self) -> None:
        satellite1Number: int = self.__satellite1.number
        satellite2Number: int = self.__satellite2.number
        satellite3Number: int = self.__satellite3.number
        print("Ионосферная задержка\nСпутник #" + str(satellite1Number) + "\t\tСпутник #" + str(satellite2Number) +
              "\t\tСпутник #" + str(satellite3Number))
        for observation in range(self.__amountOfObservations):
            delay1: float = self.__satellite1.ionosphericDelay[observation]
            delay2: float = self.__satellite2.ionosphericDelay[observation]
            delay3: float = self.__satellite3.ionosphericDelay[observation]
            print(str(round(delay1, 10)) + "\t" + str(round(delay2, 10)) + "\t" + str(round(delay3, 10)))


class GraphDrawer:
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations: int = amountOfObservations

    def showDelays(self) -> None:
        satellite1Number: int = self.__satellite1.number
        satellite2Number: int = self.__satellite2.number
        satellite3Number: int = self.__satellite3.number
        satellite1Delays: list = []
        satellite2Delays: list = []
        satellite3Delays: list = []
        observations = np.arange(0, self.__amountOfObservations)
        for observation in range(self.__amountOfObservations):
            delay1: float = self.__satellite1.ionosphericDelay[observation]
            delay2: float = self.__satellite2.ionosphericDelay[observation]
            delay3: float = self.__satellite3.ionosphericDelay[observation]
            satellite1Delays.append(delay1)
            satellite2Delays.append(delay2)
            satellite3Delays.append(delay3)
        plt.plot(observations, satellite1Delays, 'o-', label="Спутник #" + str(satellite1Number))
        plt.plot(observations, satellite2Delays, 'o-', label="Спутник #" + str(satellite2Number))
        plt.plot(observations, satellite3Delays, 'o-', label="Спутник #" + str(satellite3Number))
        plt.xlabel("Время")
        plt.ylabel("Ионосферная задержка, метры")
        plt.legend()
        plt.grid(linestyle='-', linewidth=0.5)
        plt.show()


def main():
    amountOfObservations: int = 360

    satellite1P1: list = [ ? ]

    satellite1P2: list = [ ? ]

    satellite2P1: list = [ ? ]

    satellite2P2: list = [ ? ]

    satellite3P1: list = [ ? ]

    satellite3P2: list = [ ? ]

    satellite1: Satellite = SatelliteFactory.createSatellite(?, satellite1P1, satellite1P2)
    satellite2: Satellite = SatelliteFactory.createSatellite(?, satellite2P1, satellite2P2)
    satellite3: Satellite = SatelliteFactory.createSatellite(?, satellite3P1, satellite3P2)

    consoleOutput: ConsoleOutput = ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations)
    consoleOutput.printDelays()

    graphDrawer: GraphDrawer = GraphDrawer(satellite1, satellite2, satellite3, amountOfObservations)
    graphDrawer.showDelays()


if __name__ == '__main__':
    main()
