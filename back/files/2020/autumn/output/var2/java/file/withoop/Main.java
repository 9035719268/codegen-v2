package ?;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        int amountOfObservations = 360;
        int amountOfHours = 3;

        String fileName = "?";
        FileReader fileReader = new FileReader(fileName);

        SatelliteFactory satelliteFactory = new SatelliteFactory(fileReader, amountOfObservations);

        Satellite satellite1 = satelliteFactory.createSatellite('?', ?);
        Satellite satellite2 = satelliteFactory.createSatellite('?', '?', ?);
        Satellite satellite3 = satelliteFactory.createSatellite('?', '?', ?);

        ConsoleOutput consoleOutput = new ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations, amountOfHours);
        consoleOutput.getConsoleOutput();

        GraphDrawer.draw();
    }
}


class ConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;
    private final int amountOfObservations;
    private final int amountOfHours;

    public ConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3,
                         int amountOfObservations, int amountOfHours) {
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
        this.amountOfObservations = amountOfObservations;
        this.amountOfHours = amountOfHours;
    }

    public void getConsoleOutput() {
        TemplateConsoleOutput angularVelocitiesOutput = new AngularVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        angularVelocitiesOutput.print();

        TemplateConsoleOutput averageAngularVelocitiesOutput = new AverageAngularVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfHours);
        averageAngularVelocitiesOutput.print();

        TemplateConsoleOutput linearVelocitiesOutput = new LinearVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        linearVelocitiesOutput.print();

        TemplateConsoleOutput averageLinearVelocitiesOutput = new AverageLinearVelocitiesConsoleOutput(satellite1, satellite2, satellite3, amountOfHours);
        averageLinearVelocitiesOutput.print();
    }
}


abstract class TemplateConsoleOutput {
    private final int amountOfObservations;

    public TemplateConsoleOutput(int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
    }

    protected void print() {
        int satellite1Number = getSatellite1Number();
        int satellite2Number = getSatellite2Number();
        int satellite3Number = getSatellite3Number();
        String legend = getLegend();
        System.out.println(legend);
        System.out.println("Спутник #" + satellite1Number + "\t\tСпутник #" + satellite2Number + "\t\tСпутник #" + satellite3Number);
        DecimalFormat df = new DecimalFormat("#.##########");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1Velocity = getVelocities1().get(observation);
            double satellite2Velocity = getVelocities2().get(observation);
            double satellite3Velocity = getVelocities3().get(observation);
            System.out.println(df.format(satellite1Velocity) + "\t" + df.format(satellite2Velocity) + "\t" + df.format(satellite3Velocity));
        }
        System.out.println("***********************************************");
    }

    abstract String getLegend();
    abstract int getSatellite1Number();
    abstract int getSatellite2Number();
    abstract int getSatellite3Number();
    abstract List<Double> getVelocities1();
    abstract List<Double> getVelocities2();
    abstract List<Double> getVelocities3();
}


class AngularVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public AngularVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    String getLegend() {
        String legend = "Угловая скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    List<Double> getVelocities1() {
        List<Double> velocities = satellite1.getAngularVelocities();
        return velocities;
    }

    @Override
    List<Double> getVelocities2() {
        List<Double> velocities = satellite2.getAngularVelocities();
        return velocities;
    }

    @Override
    List<Double> getVelocities3() {
        List<Double> velocities = satellite3.getAngularVelocities();
        return velocities;
    }
}


class AverageAngularVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public AverageAngularVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    String getLegend() {
        String legend = "Средняя угловая скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    List<Double> getVelocities1() {
        List<Double> velocities = satellite1.getAverageAngularVelocities();
        return velocities;
    }

    @Override
    List<Double> getVelocities2() {
        List<Double> velocities = satellite2.getAverageAngularVelocities();
        return velocities;
    }

    @Override
    List<Double> getVelocities3() {
        List<Double> velocities = satellite3.getAverageAngularVelocities();
        return velocities;
    }
}


class LinearVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public LinearVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    String getLegend() {
        String legend = "Линейная скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    List<Double> getVelocities1() {
        List<Double> velocities = satellite1.getLinearVelocities();
        return velocities;
    }

    @Override
    List<Double> getVelocities2() {
        List<Double> velocities = satellite2.getLinearVelocities();
        return velocities;
    }

    @Override
    List<Double> getVelocities3() {
        List<Double> velocities = satellite3.getLinearVelocities();
        return velocities;
    }
}


class AverageLinearVelocitiesConsoleOutput extends TemplateConsoleOutput {
    private final Satellite satellite1;
    private final Satellite satellite2;
    private final Satellite satellite3;

    public AverageLinearVelocitiesConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(amountOfObservations);
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
    }

    @Override
    String getLegend() {
        String legend = "Средняя линейная скорость";
        return legend;
    }

    @Override
    int getSatellite1Number() {
        int number = satellite1.getNumber();
        return number;
    }

    @Override
    int getSatellite2Number() {
        int number = satellite2.getNumber();
        return number;
    }

    @Override
    int getSatellite3Number() {
        int number = satellite3.getNumber();
        return number;
    }

    @Override
    List<Double> getVelocities1() {
        List<Double> velocities = satellite1.getAverageLinearVelocities();
        return velocities;
    }

    @Override
    List<Double> getVelocities2() {
        List<Double> velocities = satellite2.getAverageLinearVelocities();
        return velocities;
    }

    @Override
    List<Double> getVelocities3() {
        List<Double> velocities = satellite3.getAverageLinearVelocities();
        return velocities;
    }
}


class SatelliteFactory {
    private final int amountOfObservations;
    private final FileReader fileReader;

    public SatelliteFactory(FileReader fileReader, int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
        this.fileReader = fileReader;
    }

    public Satellite createSatellite(char satelliteNumber1, char satelliteNumber2, int satelliteNumberSize) {
        String satelliteNumberStr = Character.toString(satelliteNumber1) + Character.toString(satelliteNumber2);
        int satelliteNumber = Integer.parseInt(satelliteNumberStr);
        List<Double> elevationAnglesArray = new ArrayList<>(amountOfObservations);
        List<List<Double>> measurements = fileReader.getMeasurements(satelliteNumber1, satelliteNumber2, satelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double elevationAngle = measurements.get(observation).get(14);
            elevationAnglesArray.add(elevationAngle);
        }
        ElevationAngles elevationAngles = new ElevationAngles(elevationAnglesArray);
        AngularVelocities angularVelocities = new AngularVelocities(elevationAngles, amountOfObservations);
        LinearVelocities linearVelocities = new LinearVelocities(amountOfObservations);
        Satellite satellite = new Satellite(satelliteNumber, angularVelocities, linearVelocities);
        return satellite;
    }

    public Satellite createSatellite(char satelliteNumber, int satelliteNumberSize) {
        int satelliteNumeric = Character.getNumericValue(satelliteNumber);
        List<Double> elevationAnglesArray = new ArrayList<>(amountOfObservations);
        List<List<Double>> measurements = fileReader.getMeasurements(satelliteNumber, satelliteNumberSize);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double elevationAngle = measurements.get(observation).get(14);
            elevationAnglesArray.add(elevationAngle);
        }
        ElevationAngles elevationAngles = new ElevationAngles(elevationAnglesArray);
        AngularVelocities angularVelocities = new AngularVelocities(elevationAngles, amountOfObservations);
        LinearVelocities linearVelocities = new LinearVelocities(amountOfObservations);
        Satellite satellite = new Satellite(satelliteNumeric, angularVelocities, linearVelocities);
        return satellite;
    }
}


class Satellite {
    private final int number;
    private final AngularVelocities angularVelocities;
    private final LinearVelocities linearVelocities;

    public Satellite(int number, AngularVelocities angularVelocities, LinearVelocities linearVelocities) {
        this.number = number;
        this.angularVelocities = angularVelocities;
        this.linearVelocities = linearVelocities;
    }

    public int getNumber() {
        return number;
    }

    public List<Double> getAngularVelocities() {
        List<Double> velocities = angularVelocities.getAngularVelocities();
        return velocities;
    }

    public List<Double> getAverageAngularVelocities() {
        List<Double> velocities = angularVelocities.getAverageAngularVelocities();
        return velocities;
    }

    public List<Double> getLinearVelocities() {
        List<Double> velocities = linearVelocities.getLinearVelocities();
        return velocities;
    }

    public List<Double> getAverageLinearVelocities() {
        List<Double> velocities = linearVelocities.getAverageLinearVelocities();
        return velocities;
    }
}


class AngularVelocities {
    private final int amountOfObservations;
    private final ElevationAngles elevationAngles;
    private final List<Double> angularVelocities;

    public AngularVelocities(ElevationAngles elevationAngles, int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
        this.angularVelocities = new ArrayList<>(amountOfObservations);
        this.elevationAngles = elevationAngles;
    }

    public List<Double> getAverageAngularVelocities() {
        List<Double> averageVelocities = new ArrayList<>();
        List<Double> angularVelocities = getAngularVelocities();
        int observationsPerHour = 120;

        double firstHourSum = getVelocitySumPerHour(angularVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(angularVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(angularVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.add(firstHourAverage);
        averageVelocities.add(secondHourAverage);
        averageVelocities.add(thirdHourAverage);

        return averageVelocities;
    }

    public List<Double> getAngularVelocities() {
        double oneHourInSeconds = 3600;
        double previousElevationAngle = elevationAngles.getAngleAt(0);
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double currentElevationAngle = elevationAngles.getAngleAt(observation);
            double angularVelocity = (currentElevationAngle - previousElevationAngle) / oneHourInSeconds;
            angularVelocities.add(angularVelocity);
            previousElevationAngle = currentElevationAngle;
        }
        return angularVelocities;
    }

    private double getVelocitySumPerHour(List<Double> velocities, int start, int end) {
        double hourSum = 0;
        for (int observation = start; observation < end; observation++) {
            double velocity = velocities.get(observation);
            hourSum += velocity;
        }
        return hourSum;
    }
}


class LinearVelocities {
    private final List<Double> linearVelocities;
    private final int amountOfObservations;

    public LinearVelocities(int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
        this.linearVelocities = new ArrayList<>(amountOfObservations);
    }

    public List<Double> getAverageLinearVelocities() {
        List<Double> averageVelocities = new ArrayList<>();
        List<Double> angularVelocities = getLinearVelocities();
        int observationsPerHour = 120;

        double firstHourSum = getVelocitySumPerHour(angularVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(angularVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(angularVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.add(firstHourAverage);
        averageVelocities.add(secondHourAverage);
        averageVelocities.add(thirdHourAverage);

        return averageVelocities;
    }

    public List<Double> getLinearVelocities() {
        double gravitational = 6.67 * Math.pow(10, -11);
        double earthMass = 5.972E24;
        double earthRadius = 6_371_000;
        double flightHeight = 20_000;

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double linearVelocity = Math.sqrt(gravitational * earthMass / (earthRadius + flightHeight));
            linearVelocities.add(linearVelocity);
        }
        return linearVelocities;
    }

    private double getVelocitySumPerHour(List<Double> velocities, int start, int end) {
        double velocitySum = 0;
        for (int observation = start; observation < end; observation++) {
            velocitySum += velocities.get(observation);
        }
        return velocitySum;
    }
}


class ElevationAngles {
    private final List<Double> elevationAngles;

    public ElevationAngles(List<Double> elevationAngles) {
       this.elevationAngles = elevationAngles;
    }

    public double getAngleAt(int observation) {
        return elevationAngles.get(observation);
    }
}


class FileReader {
    private final byte[] allBytes;
    private final List<List<List<Byte>>> allLines;

    public FileReader(String fileName) throws IOException {
        FileInputStream fin = new FileInputStream(fileName);
        allBytes = new byte[fin.available()];
        int offset = 0;
        fin.read(allBytes, offset, allBytes.length);
        allLines = analyzeSyntaxAndReturnLinesList();
    }

    public List<List<Double>> getMeasurements(char requiredSatelliteNumber1, char requiredSatelliteNumber2, int requiredSatelliteNumberSize) {
        List<List<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;
        for (List<List<Byte>> line : allLines) {
            try {
                char satelliteNumber1 = (char)(byte)line.get(0).get(0);
                char satelliteNumber2 = (char)(byte)line.get(0).get(1);
                int satelliteNumberSize = line.get(0).size();

                if ((satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    List<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) { }
        }
        return measurements;
    }

    public List<List<Double>> getMeasurements(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        List<List<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;

        for (List<List<Byte>> line : allLines) {
            try {
                char satelliteNumber = (char)(byte)line.get(0).get(0);
                int satelliteNumberSize = line.get(0).size();

                if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    List<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) { }
        }
        return measurements;
    }

    private double getNumeric(List<List<Byte>> line, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int numberLength = line.get(number).size();

        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char)line.get(number).get(digit).byteValue();
            numberBuilder.append(symbol);
        }

        double numeric = Double.parseDouble(numberBuilder.toString());
        return numeric;
    }

    private List<List<List<Byte>>> analyzeSyntaxAndReturnLinesList() {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte tab = 9;
        byte newLine = 10;
        byte carriageReturn = 13;
        byte space = 32;

        for (byte symbol : allBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == tab)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }
}