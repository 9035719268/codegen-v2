package Default;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.List;

import static java.util.Arrays.asList;

public class GraphDrawer extends Application {
    public static void draw() {
        launch();
    }

    @Override
    public void start(Stage primaryStage) {
        int amountOfObservations = 12;

        AxisGeo axisGeo = new AxisGeo(UserGeo.lonpp, IgpGeo.lon1, IgpGeo.lon2, UserGeo.latpp, IgpGeo.lat1, IgpGeo.lat2);
        WeightMatrix weightMatrix = new WeightMatrix(axisGeo);

        List<Double> alphaArray = asList( 0.7451E-08, -0.1490E-07, -0.5960E-07,  0.1192E-06 );
        List<Double> betaArray = asList( 0.9216E+05, -0.1147E+06, -0.1311E+06, 0.7209E+06 );
        List<Double> gpsTimeArray = asList(
			80496.0, 90270.0, 93630.0, 100830.0, 108030.0, 0.0, 124230.0, 129630.0, 136800.0, 144000.0, 151200.0, 158400.0
		);

        IonCoefficients alpha = new IonCoefficients(alphaArray);
        IonCoefficients beta = new IonCoefficients(betaArray);
        GpsTime gpsTime = new GpsTime(gpsTimeArray);

        List<Integer> forecastA1 = asList( 31, 32, 42, 39, 30, 31, 26, 25, 19, 14, 14, 14 );
        List<Integer> forecastA2 = asList( 31, 32, 41, 39, 30, 26, 26, 24, 18, 14, 14, 16 );
        List<Integer> forecastA3 = asList( 24, 35, 49, 45, 31, 26, 26, 26, 21, 16, 13, 17 );
        List<Integer> forecastA4 = asList( 25, 36, 50, 43, 30, 25, 26, 26, 18, 14, 13, 15 );

        List<Integer> preciseA1 = asList( 25, 28, 36, 35, 24, 23, 19, 30, 15, 9, 5, 11 );
        List<Integer> preciseA2 = asList( 26, 27, 35, 36, 25, 25, 19, 31, 17, 9, 5, 12 );
        List<Integer> preciseA3 = asList( 21, 29, 43, 39, 22, 21, 21, 28, 19, 10, 4, 13 );
        List<Integer> preciseA4 = asList( 20, 30, 43, 37, 20, 19, 21, 28, 17, 11, 4, 11 );

        List<IonosphericDelay> forecastDelays = IonosphericDelaysFactory.createDelays(weightMatrix, forecastA1, forecastA2, forecastA3, forecastA4,
                                                                                      amountOfObservations);
        List<IonosphericDelay> preciseDelays = IonosphericDelaysFactory.createDelays(weightMatrix, preciseA1, preciseA2, preciseA3, preciseA4,
                                                                                     amountOfObservations);
        List<KlobucharModel> klobucharDelays = KlobucharDelaysFactory.createKlobuchar(gpsTime, alpha, beta, amountOfObservations);

        init(primaryStage, forecastDelays, preciseDelays, klobucharDelays, amountOfObservations);
    }

    @SuppressWarnings("unchecked")
    private void init(Stage primaryStage, List<IonosphericDelay> forecastDelays, List<IonosphericDelay> preciseDelays, List<KlobucharModel> klobucharDelays,
                      int amountOfObservations) {
        HBox root = new HBox();
        Scene scene = new Scene(root, 450, 330);

        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Время, час");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Ионосферная поправка, метр");

        LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        XYChart.Series<Number, Number> forecastData = new XYChart.Series<>();
        forecastData.setName("igrg");
        XYChart.Series<Number, Number> preciseData = new XYChart.Series<>();
        preciseData.setName("igsg");
        XYChart.Series<Number, Number> klobucharData = new XYChart.Series<>();
        klobucharData.setName("Klobuchar");

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double forecastDelay = forecastDelays.get(observation).getDelayInMeters();
            forecastData.getData().add(new XYChart.Data<>(observation, forecastDelay));

            double preciseDelay = preciseDelays.get(observation).getDelayInMeters();
            preciseData.getData().add(new XYChart.Data<>(observation, preciseDelay));

            double klobucharDelay = klobucharDelays.get(observation).getKlobucharDelayInMeters();
            klobucharData.getData().add(new XYChart.Data<>(observation, klobucharDelay));
        }

        lineChart.getData().addAll(forecastData, preciseData, klobucharData);
        root.getChildren().add(lineChart);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
