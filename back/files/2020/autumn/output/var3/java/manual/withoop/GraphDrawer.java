package

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.util.List;

import static java.util.Arrays.asList;

?;

public class GraphDrawer extends Application {
    public static void draw() {
        launch();
    }

    @Override
    public void start(Stage primaryStage) {
        int amountOfObservations = 12;

        AxisGeo axisGeo = new AxisGeo(UserGeo.lonpp, IgpGeo.lon1, IgpGeo.lon2, UserGeo.latpp, IgpGeo.lat1, IgpGeo.lat2);
        WeightMatrix weightMatrix = new WeightMatrix(axisGeo);

        List<Double> alphaArray = asList( ? );
        List<Double> betaArray = asList( ? );
        List<Double> gpsTimeArray = asList( ? );

        IonCoefficients alpha = new IonCoefficients(alphaArray);
        IonCoefficients beta = new IonCoefficients(betaArray);
        GpsTime gpsTime = new GpsTime(gpsTimeArray);

        List<Integer> forecastA1 = asList( ? );
        List<Integer> forecastA2 = asList( ? );
        List<Integer> forecastA3 = asList( ? );
        List<Integer> forecastA4 = asList( ? );

        List<Integer> preciseA1 = asList( ? );
        List<Integer> preciseA2 = asList( ? );
        List<Integer> preciseA3 = asList( ? );
        List<Integer> preciseA4 = asList( ? );

        List<IonosphericDelay> forecastDelays = IonosphericDelaysFactory.createDelays(weightMatrix, forecastA1, forecastA2, forecastA3, forecastA4,
                                                                                      amountOfObservations);
        List<IonosphericDelay> preciseDelays = IonosphericDelaysFactory.createDelays(weightMatrix, preciseA1, preciseA2, preciseA3, preciseA4,
                                                                                     amountOfObservations);
        List<KlobucharModel> klobucharDelays = KlobucharDelaysFactory.createKlobuchar(gpsTime, alpha, beta, amountOfObservations);

        init(primaryStage, forecastDelays, preciseDelays, klobucharDelays, amountOfObservations);
    }

    @SuppressWarnings("unchecked")
    private void init(Stage primaryStage, List<IonosphericDelay> forecastDelays, List<IonosphericDelay> preciseDelays, List<KlobucharModel> klobucharDelays,
                      int amountOfObservations) {
        HBox root = new HBox();
        Scene scene = new Scene(root, 450, 330);

        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Время, час");

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Ионосферная поправка, метр");

        LineChart<Number, Number> lineChart = new LineChart<>(xAxis, yAxis);
        XYChart.Series<Number, Number> forecastData = new XYChart.Series<>();
        forecastData.setName("igrg");
        XYChart.Series<Number, Number> preciseData = new XYChart.Series<>();
        preciseData.setName("igsg");
        XYChart.Series<Number, Number> klobucharData = new XYChart.Series<>();
        klobucharData.setName("Klobuchar");

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double forecastDelay = forecastDelays.get(observation).getDelayInMeters();
            forecastData.getData().add(new XYChart.Data<>(observation, forecastDelay));

            double preciseDelay = preciseDelays.get(observation).getDelayInMeters();
            preciseData.getData().add(new XYChart.Data<>(observation, preciseDelay));

            double klobucharDelay = klobucharDelays.get(observation).getKlobucharDelayInMeters();
            klobucharData.getData().add(new XYChart.Data<>(observation, klobucharDelay));
        }

        lineChart.getData().addAll(forecastData, preciseData, klobucharData);
        root.getChildren().add(lineChart);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
